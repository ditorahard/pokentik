import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {SignedIn, SignedOut, createRootNavigator} from './routes';
import NavigationService from './routes/NavigationService';
import deviceStorage from "./services/deviceStorage";
import firebase from 'react-native-firebase';
import type { Notification, NotificationOpen } from 'react-native-firebase';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      signedIn: false,
      checkedSignIn: false
    };
  } 


  componentDidMount() {

        let notif = new Promise ((rs, rj) => {
          this.getFirebaseNotifications();
        }).then((response) => {
          console.log("Anjay");
        });
        

    deviceStorage.isSignedIn()
    .then(res => this.setState({ signedIn: res, checkedSignIn: true }))
    .catch(err => alert("An error occurred"));
  }

  async getFirebaseNotifications(){
    const notificationOpen: NotificationOpen = await firebase.notifications().getInitialNotification();
        
        if (notificationOpen) {
            const action = notificationOpen.action;
            const notification: Notification = notificationOpen.notification;
            var seen = [];
            alert(JSON.stringify(notification.data, function(key, val) {
                if (val != null && typeof val == "object") {
                    if (seen.indexOf(val) >= 0) {
                        return;
                    }
                    seen.push(val);
                }
                return val;
            }));
          }
      const channel = new firebase.notifications.Android.Channel('test-channel', 'Test Channel', firebase.notifications.Android.Importance.Max)
                .setDescription('My apps test channel');
// Create the channel
        firebase.notifications().android.createChannel(channel);
        this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {
            // Process your notification as required
            // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
        });
        this.notificationListener = firebase.notifications().onNotification((notification: Notification) => {
            // Process your notification as required
            notification
                .android.setChannelId('test-channel')
                .android.setSmallIcon('ic_launcher');
            firebase.notifications()
                .displayNotification(notification);
            
        });
        this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
            // Get the action triggered by the notification being opened
            const action = notificationOpen.action;
            // Get information about the notification that was opened
            const notification: Notification = notificationOpen.notification;
            var seen = [];
            alert(JSON.stringify(notification.data, function(key, val) {
                if (val != null && typeof val == "object") {
                    if (seen.indexOf(val) >= 0) {
                        return;
                    }
                    seen.push(val);
                }
                return val;
            }));
            firebase.notifications().removeDeliveredNotification(notification.notificationId);
            
        });
  }

  componentWillUnmount() {
        this.notificationDisplayedListener();
        this.notificationListener();
        this.notificationOpenedListener();
    }

  render () {
  	const { checkedSignIn, signedIn } = this.state;
  	const Layout = createRootNavigator(signedIn);
  	if (!checkedSignIn) {
      return (<View>
      	<Text>Pokentik Loading ...</Text>
      </View>);
    }
     if (signedIn) {
      return <SignedIn 
	      ref={navigatorRef => {
		    NavigationService.setTopLevelNavigator(navigatorRef);
		  }} 
      />;

    } else {
        return <SignedOut 
          ref={navigatorRef => {
    	    NavigationService.setTopLevelNavigator(navigatorRef);
    	  }} 
      />;

    }
   //  else{
	  //   return 
	  //   	(<Layout 
	  //   	ref={navigatorRef => {
	  //       NavigationService.setTopLevelNavigator(navigatorRef);
	  //       }} 
	  //      />);
	  // }
	}
}

export default App;