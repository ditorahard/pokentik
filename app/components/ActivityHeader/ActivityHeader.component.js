import React from 'react';
import { View, Text, Image } from 'react-native';
import styles from './ActivityHeader.component.style';
import PropTypes from 'prop-types';	
import ImageResizeMode from 'react-native/Libraries/Image/ImageResizeMode';

export default class ActivityHeader extends React.PureComponent {
  render() {
    return (
    <View>
      {this.props.image?
        <Image
            source={{uri: this.props.image}}
            style={{
              height: 200,
              alignSelf: "stretch",
              justifyContent: "center",
              alignItems: "stretch",
            }}
             resizeMode={ImageResizeMode.stretch}
            >
      </Image> :
    <View style={styles.hero}>
	    <Text style={styles.whitetext}>
	        {this.props.title}
	    </Text>
      {this.props.subtitle? 
        <Text style={styles.whitetextSubtitle}>
          {this.props.subtitle}
        </Text> 
        : null
      }
	  </View>}
  </View>
    );
  }
}


ActivityHeader.propTypes = {
  title: PropTypes.string,
  image: PropTypes.string,
  subtitle: PropTypes.string
}