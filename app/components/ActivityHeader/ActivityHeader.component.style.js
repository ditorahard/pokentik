import {StyleSheet} from 'react-native';
// import theme from '../../styles/theme.style';
// import {headingText, textInput} from '../../styles/common.style';

export default StyleSheet.create({
	hero:{
		padding:20,
		// paddingTop:30,
		alignItems:'center',
		backgroundColor:'#18B5AE',
	},
	whitetext:{
		color:'#FFFFFF',
		fontSize:30,
		fontWeight:'700',
	},
	whitetextSubtitle:{
		color: '#FFFFFF',
		fontSize:15,
	}
});