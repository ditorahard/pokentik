import React from 'react';
import { TouchableOpacity, View, Image } from 'react-native';
import { Text, Icon } from 'native-base';
import style from './Ad.component.style';
import PropTypes from 'prop-types';	
import ImageResizeMode from 'react-native/Libraries/Image/ImageResizeMode';

export default class Ad extends React.PureComponent {
  render() {
    return (
    	  <TouchableOpacity onPress={this.props.action}>
          {this.props.imageURI ?
           <View style={style.imageContainer}> 
           <Image
                source={this.props.imageURI}
                style={style.image}
                 resizeMode={ImageResizeMode.stretch}
                >
          </Image>
          </View> :
          <View style={style.card}>
            <View>
              <Text>
                {this.props.text}
              </Text>
            </View>
          </View>
        }
          
        </TouchableOpacity>
    );
  }
}


Ad.propTypes = {
  text: PropTypes.string,
  action : PropTypes.func,
  imageURI: PropTypes.object
  // content: PropTypes.array.isRequired,
  // size: PropTypes.number.isRequired,
  // styles: PropTypes.object.isRequired,
  // isReady: PropTypes.bool.isRequired,
}
