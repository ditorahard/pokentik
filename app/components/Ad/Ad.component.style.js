import {StyleSheet} from 'react-native';
// import theme from '../../styles/theme.style';
// import {headingText, textInput} from '../../styles/common.style';
import SCREEN_IMPORT from 'Dimensions';
  
const SCREEN_WIDTH = SCREEN_IMPORT.get('window').width;
const SCREEN_HEIGHT = SCREEN_IMPORT.get('window').height;

export default StyleSheet.create({
	card:{
		backgroundColor: "#18B5AE",
		elevation:4,
		shadowOffset: { width: 5, height: 5 },
		shadowColor: "grey",
		shadowOpacity: 0.5,
		shadowRadius: 10,
		marginBottom:3,
		marginTop:3,
		padding:25,
		marginRight:10,
		width:80,
	},
	imageContainer:{
		marginTop:10,
		marginLeft:10,
		marginRight:10,
	},
	image:{
		width: SCREEN_WIDTH-20,
		height: 120,
		resizeMode:'stretch',
	}
});