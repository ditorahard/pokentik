import {StyleSheet} from 'react-native';
// import theme from '../../styles/theme.style';
// import {headingText, textInput} from '../../styles/common.style';

export default StyleSheet.create({
	icon:{
		alignItems: 'center', 
		justifyContent: 'center',
	},
	iconButton:{
		color:"#18B5AE",
	},
	buttonContainer:{
		width:80,
		padding:10,
		// borderRightWidth:1
	},
	buttonText:{
		fontSize:10,
		textAlign:'center',
	},
	textContainer:{
		alignItems:'center',
	},
	card:{
		// backgroundColor: "#ffffff",
		// elevation:4,
		// shadowOffset: { width: 5, height: 5 },
		// shadowColor: "grey",
		// shadowOpacity: 0.5,
		// shadowRadius: 10,
		// marginBottom:3,
		// marginTop:3,
		// padding:25,
		// marginRight:10,
	},
});