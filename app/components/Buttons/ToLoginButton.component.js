import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import NavigationService from '../../routes/NavigationService';

export default class ToLoginButton extends React.Component {
  render() {
    return (
    	<TouchableOpacity onPress={() => { NavigationService.navigate('Login'); }}>
    		<Text>Login</Text>
    	</TouchableOpacity>
    );
  }
}