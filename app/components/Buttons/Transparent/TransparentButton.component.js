import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { Text, Icon } from 'native-base';
import style from './TransparentButton.component.style';
import PropTypes from 'prop-types';	

export default class TransparentButton extends React.PureComponent {
  render() {
    return (
    	  <TouchableOpacity onPress={this.props.action}>
          <View style={style.buttonContainer}>
              <View style={style.icon}>
                <Icon style={{color:this.props.iconColor}} name={this.props.icon} />
              </View>
              <View style={style.textContainer}>
                <Text style={style.buttonText}>
                  {this.props.text}
                </Text>
              </View>
          </View>
        </TouchableOpacity>
    );
  }
}


TransparentButton.propTypes = {
  text: PropTypes.string,
  action : PropTypes.func,
  icon: PropTypes.string,
  iconColor: PropTypes.string,
  // content: PropTypes.array.isRequired,
  // size: PropTypes.number.isRequired,
  // styles: PropTypes.object.isRequired,
  // isReady: PropTypes.bool.isRequired,
}