import {StyleSheet} from 'react-native';
import SCREEN_IMPORT from 'Dimensions';
  
const SCREEN_WIDTH = SCREEN_IMPORT.get('window').width/5;
const SCREEN_HEIGHT = SCREEN_IMPORT.get('window').height;
// import theme from '../../styles/theme.style';
// import {headingText, textInput} from '../../styles/common.style';

export default StyleSheet.create({
	icon:{
		alignItems: 'center', 
		justifyContent: 'center',
	},
	iconButton:{
		color:"#18B5AE",
	},
	buttonContainer:{
		flex:1,
		width:SCREEN_WIDTH+5,
		padding:10,
		margin:5,
		borderWidth:1,
		borderRadius:10,
		borderColor:'grey'
	},
	buttonText:{
		fontSize:10,
		textAlign:'center',
	},
	textContainer:{
		alignItems:'center',
	},
	card:{
		// backgroundColor: "#ffffff",
		// elevation:4,
		// shadowOffset: { width: 5, height: 5 },
		// shadowColor: "grey",
		// shadowOpacity: 0.5,
		// shadowRadius: 10,
		// marginBottom:3,
		// marginTop:3,
		// padding:25,
		// marginRight:10,
	},
});