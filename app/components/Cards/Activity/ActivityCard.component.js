import React, { Component } from 'react';
import { Image, TouchableOpacity } from 'react-native';
import { Container, /*Header,*/ Content,  Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import styles from './ActivityCard.component.style';
import PropTypes from 'prop-types';

export default class ActivityCard extends Component {
  render() {
    return (
          <Card style={styles.card}>
            <TouchableOpacity
                   onPress={this.props.action}>
                <CardItem cardBody>
                  <Image source={this.props.image!=null&&this.props.image!='' ? {uri: this.props.image} : require('../../../assets/img/cardtop.png')} style={styles.cardImage}/>
                </CardItem>
            </TouchableOpacity>
            <CardItem>
              <Left>
                <Body>
                  <TouchableOpacity
                     onPress={this.props.action}>
                    <Text>{this.props.title}</Text>
                    <Text note>{this.props.subtitle}</Text>
                  </TouchableOpacity>
                </Body>
              </Left>
              <Right>
                <TouchableOpacity
                   onPress={this.props.actionShare}>
                      <Icon style={styles.iconButton} name='md-share' />
                </TouchableOpacity>
              </Right>
            </CardItem>
          </Card>
    );
  }
}

ActivityCard.propTypes = {
  title: PropTypes.string,
  image: PropTypes.string,
  subtitle: PropTypes.string,
  action : PropTypes.func,
  actionShare: PropTypes.func,
}