import {StyleSheet} from 'react-native';
import SCREEN_IMPORT from 'Dimensions';
  
const SCREEN_WIDTH = SCREEN_IMPORT.get('window').width
// const SCREEN_HEIGHT = SCREEN_IMPORT.get('window').height,
// import theme from '../../styles/theme.style';
// import {headingText, textInput} from '../../styles/common.style';

export default StyleSheet.create({
	roundedForm:{
		flex:1,
	},
	card:{
		//width:260,
	},
	cardImage:{
		height: 200, 
		width: SCREEN_WIDTH, 
		flex: 1,
		alignSelf: "stretch",
        justifyContent: "center",
        alignItems: "stretch",
        resizeMode:"stretch",
	},
	shareButton:{

	},
	iconButton:{
		color: 'black'
	},
});