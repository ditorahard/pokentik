import React, { Component } from 'react';
import { Container, Header, Content, Form, Item, Picker, Icon } from 'native-base';
// import styles from './UnderlinePicker.component.style';
import PropTypes from 'prop-types';

export default class UnderlinePicker extends Component {
  render() {
  	var thisEntity = this;
  	var pickerlist = null;
  	if (this.props.options) {
            pickerlist = this.props.options.map(function (item, i) { 
                return (<Picker.Item key={i} label={item.name} value={item.id} />);
        });
    }

    return (
            <Item picker>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="ios-arrow-down-outline" />}
                style={{ width: undefined }}
                placeholder="Pilih"
                placeholderStyle={{ color: "#bfc6ea" }}
                placeholderIconColor="#007aff"
                selectedValue={this.props.selected}
                onValueChange={this.props.onValueChange}
              >
                {pickerlist}
              </Picker>
            </Item>
    );
  }
}

UnderlinePicker.propTypes = {
  options: PropTypes.array,
  onValueChange : PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired,
}

