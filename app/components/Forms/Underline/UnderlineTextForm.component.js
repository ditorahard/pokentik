import React, { Component } from 'react';
import { Item, Input } from 'native-base';
import styles from './UnderlineTextForm.component.style';
import PropTypes from 'prop-types';

export default class UnderlineTextForm extends Component {
  render() {
    return (
          <Item disabled={this.props.disabled ? this.props.disabled : false} style={styles.underlinedForm}>
            <Input disabled={this.props.disabled ? this.props.disabled : false} placeholder={this.props.placeholder} 
            secureTextEntry={this.props.secure} 
            onChangeText={this.props.onChangeText}
            value={this.props.value}
            keyboardType={this.props.keyboardType ? this.props.keyboardType : "default"}/>
          </Item>
    );
  }
}

UnderlineTextForm.propTypes = {
  placeholder: PropTypes.string.isRequired,
  secure: PropTypes.bool,
  value: PropTypes.string,
  onChangeText : PropTypes.func,
  keyboardType: PropTypes.string,
  disabled: PropTypes.bool,
}