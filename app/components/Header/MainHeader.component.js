import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { Header, Left, Body, Right, Button, Icon, Title, Text } from 'native-base';
import { StackNavigator } from "react-navigation";
import PropTypes from 'prop-types';
import style from './MainHeader.component.style.js';
import { withNavigation } from 'react-navigation';
import NavigationService from '../../routes/NavigationService';

class MainHeader extends Component {
  render() {
    return (
        <Header style={style.whiteHeader} androidStatusBarColor="#000000">
          <Left>
            { this.props.title ? 
            ( this.props.noBack ? null : <Button transparent onPress={this.props.back} onPress={() => { this.props.navigation.goBack() }}>
              <Icon style={style.iconButton} name='ios-arrow-round-back' />
            </Button>) :
              (<Button transparent onPress={this.props.openDrawer}>
              <Icon style={style.iconButton} name='menu' />
            </Button>) }
          </Left>
          <Body>
              {this.props.title ? 
                (<Text style={style.titleHeader}>
                  {this.props.title}
                  </Text>) 
                :                 
                (<View style={style.logoContainer}>
                    <Image
                    style={style.logopokentik}
                    source={require('../../assets/img/pokentikwhite.png')}
                    resizeMode="contain">
                    </Image>
                  </View>
                  )
              }
          </Body>
          {this.props.title ? 
            null
            :
            (<Right>
            <Button transparent onPress={() => { NavigationService.navigate('Notifications'); }}>
              <Icon style={style.iconButton} name='md-notifications' />
            </Button>
          </Right>)
          }
        </Header>
    );
  }
}

MainHeader.propTypes = {
  openDrawer: PropTypes.func,
  notification: PropTypes.func,
  title: PropTypes.string,
  back: PropTypes.func,
};

export default withNavigation(MainHeader);