import {StyleSheet} from 'react-native';
// import theme from '../../styles/theme.style';
// import {headingText, textInput} from '../../styles/common.style';

export default StyleSheet.create({
	whiteHeader:{
		backgroundColor: "white",
		elevation:4,
		shadowOffset: { width: 5, height: 5 },
		shadowColor: "grey",
		shadowOpacity: 0.5,
		shadowRadius: 10,
		marginBottom:3,
	},
	iconButton:{
		color:"#18B5AE",
	},
	logoContainer:{
		flex:1,
		width:null,
		height:20,
	},
	logopokentik:{
		 flex: 1,
		 width:120,

	},
	titleHeader:{
		color:'#18B5AE',
		fontSize:20,
		fontWeight:'700',
	}
});