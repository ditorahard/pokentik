import React from 'react';
import { View } from 'react-native';
import styles from './CourseHero.component.style';
import PropTypes from 'prop-types';	
import { Button, Text } from 'native-base';

export default class CourseHero extends React.PureComponent {
  render() {
    return (
    <View style={styles.hero}> 
      <View style={styles.heroContent}>
  	    <Text style={styles.whitetext}>
  	        {this.props.title}
  	    </Text>
      </View>
      <View style={styles.heroContent}>
        <Button primary>
          <Text style={styles.}>
            Start Learning
          </Text>
        </Button>
      </View>
	  </View>
    );
  }
}


CourseHero.propTypes = {
  title: PropTypes.string.isRequired,
  // action : PropTypes.func.isRequired,
  // content: PropTypes.array.isRequired,
  // size: PropTypes.number.isRequired,
  // styles: PropTypes.object.isRequired,
  // isReady: PropTypes.bool.isRequired,
}