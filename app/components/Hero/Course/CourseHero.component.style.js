import {StyleSheet} from 'react-native';
// import theme from '../../styles/theme.style';
// import {headingText, textInput} from '../../styles/common.style';

export default StyleSheet.create({
	hero:{
		paddingTop:20,
		paddingBottom:20,
		alignItems:'center',
		backgroundColor:'#014DB2',
	},
	heroContent:{
		marginBottom:10,
	},
	whitetext:{
		color:'#FFFFFF',
		fontSize:20,
		fontWeight:'700',
	},
});