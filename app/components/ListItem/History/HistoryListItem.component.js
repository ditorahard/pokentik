import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import styles from './HistoryListItem.component.style';
import PropTypes from 'prop-types';	
import { ListItem, Icon, Button, Text, Left, Body, Right } from 'native-base';

export default class HistoryListItem extends React.PureComponent {
	render() {
    	return (
			<ListItem noIndent>
					<View style={styles.historyListContainer}>
			            <View style={styles.historyTitle}>
				            <TouchableOpacity onPress={this.props.action}>
					            <Text style={styles.listHeader}>
					              	{this.props.title.length > 23 ? this.props.title.substring(0,25)+"..." : this.props.title}
					            </Text>
					            <Text style={styles.listMessage}>
					              	{this.props.subtitle}
					            </Text>
				            </TouchableOpacity>
			            </View>
		            	<View style={styles.share}>
			            	<TouchableOpacity onPress={this.props.actionShare}>
			            		<Icon style={styles.iconButton} name='md-share' />
				            	<Text style={styles.iconText}>
				            	    Share
				            	</Text>
			            	</TouchableOpacity>
		            	</View>
	            	</View>
	        </ListItem>
	        )
    }
}

HistoryListItem.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  actionShare: PropTypes.func,
  action: PropTypes.func
}