import {StyleSheet, Dimensions} from 'react-native';
// import theme from '../../styles/theme.style';
// import {headingText, textInput} from '../../styles/common.style';
var width = Dimensions.get('window').width;
var widthTitle = Dimensions.get('window').width - 100;

export default StyleSheet.create({
	listHeader:{
		fontSize:14,
		fontWeight:'700',
		alignSelf:"flex-start",
		color:"#0E0E0E"
	},
	listMessage:{
		fontSize:12,
		alignSelf:"flex-start",
		color:"#808080"
	},
	historyTitle:{
		width:widthTitle
	},
	historyListContainer:{
		flexDirection:'row',
		width:width,
	},
	share:{
		marginLeft:25,
		marginTop:5,
	},
	iconButton:{
		fontSize:10,
	},
	iconText:{
		fontSize:11,
	},
});