import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import styles from './NotificationListItem.component.style';
import PropTypes from 'prop-types';	
import { ListItem, Icon, Button, Text, Left, Body, Right } from 'native-base';

export default class NotificationListItem extends React.PureComponent {
	render() {
    	return (
			<ListItem noIndent style={styles.listItem}>
				<TouchableOpacity onPress={this.props.action}>
		            <View style={styles.listContent}>
		              <Text style={styles.listHeader}>
		              	{this.props.title}
		              </Text>
		              <Text style={styles.listMessage}>
		              	{this.props.content}
		              </Text>
		            </View>
	            {/*<Right>
	            	<Text>
	            	    5:00
	            	</Text>
	            </Right>*/}
	        	</TouchableOpacity>
	        </ListItem>
	        )
    }
}

NotificationListItem.propTypes = {
  title: PropTypes.string,
  action: PropTypes.func,
  content: PropTypes.string,
}