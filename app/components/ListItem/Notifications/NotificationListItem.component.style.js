import {StyleSheet, Dimensions} from 'react-native';
// import theme from '../../styles/theme.style';
// import {headingText, textInput} from '../../styles/common.style';

var width = Dimensions.get('window').width;

export default StyleSheet.create({
	listHeader:{
		fontSize:16,
		fontWeight:'700',
		alignSelf: 'stretch',
		color:"#0E0E0E"
	},
	listMessage:{
		fontSize:14,
		alignSelf: 'stretch',
		color:"#808080"
	},
	listContent:{
		width:width,
	}
});