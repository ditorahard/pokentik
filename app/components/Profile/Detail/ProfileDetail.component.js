import React, { Component } from 'react';
import { View, Image, Text } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title } from 'native-base';
import PropTypes from 'prop-types';
import styles from './ProfileDetail.component.style.js';

export default class ProfileDetail extends Component {
  render() {
    return (
        <View style={styles.profile}>
          <View style={styles.imageContainer}>
            <Image
              style={styles.profileImage}
              source={this.props.profileImage ? {uri : 'http://pokentik.com/public/profiles/' + this.props.profileImage} : require('../../../assets/img/fotoprofil.png')}
            />
          </View>
          <View style={styles.nameContainer}>
            <Text style={styles.profileName}>
              {this.props.name ? this.props.name : "Loading..."}
            </Text>
          </View>
          <View style={styles.detailContainer}>
            <Text style={styles.profileDetail}>
              {this.props.email ? this.props.email : "Loading..." }
            </Text>
          </View>
          <View style={styles.detailContainer}>
            <Text style={styles.profileDetail}>
             {this.props.points!=null ? this.props.points+" pts" : "Loading..."}
            </Text>
          </View>
        </View>
    );
  }
}

ProfileDetail.propTypes = {
  openDrawer: PropTypes.func,
  name: PropTypes.string,
  email: PropTypes.string,
  points: PropTypes.number,
  profileImage: PropTypes.string
};