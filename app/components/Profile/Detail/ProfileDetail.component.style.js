import {StyleSheet} from 'react-native';
// import theme from '../../styles/theme.style';
// import {headingText, textInput} from '../../styles/common.style';

export default StyleSheet.create({
	profile:{
		backgroundColor:"#18B5AE",
		color:"#ffffff",
		paddingTop:10,
		paddingBottom:20,
	},
	profileImage:{
		width: 100, 
		height: 100,
		borderRadius:50
	},
	profileName:{
		marginTop:10,
		fontSize:23,
		color:'white',
	},
	nameContainer:{
		alignItems:'center',
	},
	imageContainer:{
		paddingTop:10,
		alignItems:'center',
	},
	centerContainer:{
		alignItems:'center',
	},
	detailContainer:{
		marginTop:2,
		alignItems:'center',
	},
	profileDetail:{
		color:'white',
		fontSize:15,
	},
});