import React from "react";
import { AppRegistry, Image, StatusBar } from "react-native";
import { Container, Content, Text, List, ListItem } from "native-base";
import PropTypes from 'prop-types';
import NavigationService from '../../routes/NavigationService';
import deviceStorage from '../../services/deviceStorage';
import ImageResizeMode from 'react-native/Libraries/Image/ImageResizeMode'

// const routes = ["Home", "Profile", "Logout"];

export default class SideBar extends React.Component {
  render() {
    return (
      <Container>
        <Content>
          <Image
            source={require('../../assets/img/sidebartop.png')}
            style={{
              height: 120,
              alignSelf: "stretch",
              justifyContent: "center",
              alignItems: "center",
            }}
            resizeMode={ImageResizeMode.cover}
            >
          </Image>
          <List>
            <ListItem
              button
              onPress={() => {
                this.props.closeSidebar();
                NavigationService.navigate('Home');}}>
              <Text>Home</Text>
            </ListItem>
            <ListItem
              button
              onPress={() => {
                this.props.closeSidebar();
                NavigationService.navigate('ActivityList');
              }}>
              <Text>Daftar Aktivitas</Text>
            </ListItem>
            <ListItem
              button
              onPress={() => {
                this.props.closeSidebar();
                NavigationService.navigate('AddActivity');}}>
              <Text>Add Aktivitas</Text>
            </ListItem>
            <ListItem
              button
              onPress={() => {
                this.props.closeSidebar();
                NavigationService.navigate('Profile');}}>
              <Text>Profil</Text>
            </ListItem>
            <ListItem
              button
              onPress={() => {
                this.props.closeSidebar();
                deviceStorage.removeItem('token').then((response) => {
                    NavigationService.navigate('Logout', {message:"Anda telah berhasil logout. Anda bisa langsung menutup aplikasi. Sampai berjumpa kembali.",header:" ",title:"Logged Out",});
                  });
              }}>  
                                
              <Text>Logout</Text>
            </ListItem>
          </List>
          {
          // <List
          //   dataArray={routes}
          //   renderRow={data => {
          //     return (
          //       <ListItem
          //         button
          //         onPress={() => this.props.navigation.navigate(data)}>
          //         <Text>{data}</Text>
          //       </ListItem>
          //     );
          //   }}
          // />
          }
        </Content>
      </Container>
    );
  }
}

SideBar.propTypes = {
  logout: PropTypes.func,
  closeSidebar: PropTypes.func,
};