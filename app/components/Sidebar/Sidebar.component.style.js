import {StyleSheet} from 'react-native';
// import theme from '../../styles/theme.style';
// import {headingText, textInput} from '../../styles/common.style';

export default StyleSheet.create({
	hero:{
		paddingTop:5,
		paddingBottom:5,
		alignItems:'center',
		backgroundColor:'#18B5AE',
	},
	whitetext:{
		color:'#FFFFFF',
		fontSize:13,
		fontWeight:'700',
	},
});