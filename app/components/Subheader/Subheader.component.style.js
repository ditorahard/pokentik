import {StyleSheet} from 'react-native';
// import theme from '../../styles/theme.style';
// import {headingText, textInput} from '../../styles/common.style';

export default StyleSheet.create({
	hero:{
		paddingTop:1,
		paddingBottom:1,
		alignItems:'center',
		backgroundColor:'#EEEEEE',
	},
	whitetext:{
		color:'#FFFFFF',
		fontSize:13,
		fontWeight:'700',
	},
});