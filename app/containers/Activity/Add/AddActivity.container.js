import React, {Component} from 'react';
import { KeyboardAvoidingView, View, Image, ToastAndroid } from 'react-native';
import { Container, Content, Text, Body, Drawer, Button, Icon, Input, Item, ListItem, Left, Right, Spinner } from "native-base";
import Subheader from '../../../components/Subheader/Subheader.component';
import SideBar from '../../../components/Sidebar/Sidebar.component';
import Searchbar from '../../../components/Searchbar/Searchbar.component';
import UnderlinePicker from '../../../components/Forms/Underline/UnderlinePicker.component';
import UnderlinePickerActivity from '../../../components/Forms/UnderlineForActivity/UnderlinePickerActivity.component';
import UnderlineTextForm from '../../../components/Forms/Underline/UnderlineTextForm.component';
import MainHeader from '../../../components/Header/MainHeader.component';
import styles from './AddActivity.container.style';
import deviceStorage from '../../../services/deviceStorage';
import axios from 'axios';
import moment from 'moment';
import ImagePicker from 'react-native-image-picker';
import Sound from 'react-native-sound';
import MapView, { Marker } from 'react-native-maps';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import NavigationService from '../../../routes/NavigationService';

export default class AddActivity extends React.Component {
  constructor(props) {
        super(props);
        this.state = {
          formField:1,
          userId:'',
          negara:'Indonesia',
          alamat:'',
          provinsi:'',
          provinsiOptions:[],
          kab_kota:'',
          kab_kotaOptions:[
            {name:'Pilih provinsi terlebih dahulu', id:'99999'},
          ],
          kecamatan:'',
          kecamatanOptions:[
            {name:'Pilih kabupaten terlebih dahulu', id:'99999'},
          ],
          kelurahan:'',
          kelurahanOptions:[
            {name:'Pilih kecamatan terlebih dahulu', id:'99999'},
          ],
          rt:'',
          rw:'',
          wadah:1,
          wadahOptions:[
            {
              name:"Bak mandi", id:"1"
            }, 
            {
              name:"Bak wc", id:"2"
            },
            {
              name:"Tempayan", id:"3"
            },
            {
              name:"Ember", id:"4"
            },
            {
              name:"Dispenser", id:"5"
            },
            {
              name:"Pot bunga", id:"6"
            },
            {
              name:"Kolam", id:"7"
            },
            {
              name:"Ban bekas", id:"8"
            },
            {
              name:"Botol bekas", id:"9"
            },
            {
              name:"Lain-lain", id:"1"
            },
          ],
          jumlahWadah:'',
          tempat:1,
          tempatOptions:[
            {
              name:"Tempat tinggal", id:"1"
            },
            {
              name:"Tempat Umum", id:"2"
            },
        ],
          tipeTempat:1,
          tipeTempatOptions:[
            {
              name:"Indoor", id:"1",
            },
            {
              name:"Outdoor", id:"2",
            },
          ],
          adaJentik:1,
          adaJentikOptions:[
            {
              name:"Ya", id:"1",
            },
            {
              name:"Tidak", id:"2",
            },
          ],
          jentikOptions:[
            {
              name:"Jenis jentik", id:"Tidak diketahui"
            },
            {
              name:"Aedes Aegypti", id:"Aedes Aegypti"
            },
            {
              name:"Anopheles", id:"Anopheles",
            },
            {
              name:"Culex sp", id:"Culex sp",
            },
          ],
          jentik:'Tidak diketahui',
          tanggalObservasi:"2017-10-10",
          // latitude:-2.91402980,
          // longitude: 104.69506000,
          latitude: null,
          longitude: null,
          latitudeDelta: 0.00922,
          longitudeDelta: 0.00421,
          imageSebelum: null,
          imageSesudah: null,
          imageLoadingSebelum:false,
          imageLoadingSesudah:false,
          submitted: false,
          mapView:true,
          isLoading:false,
          regionSet:true,
          error:null,
        }
  }

  componentDidMount(){
    console.warn("Images ", this.state.imageSebelum, this.state.imageSesudah)
    let newDate = moment().format('YYYY-MM-DD');
    this._getProvinsiFirst();
    this._getKab_KotaFirst(11);
    this._getKecamatanFirst(1);
    this._getKelurahanFirst(25);
    this.setState({
      tanggalObservasi:newDate
    });
    deviceStorage.takeItem('id').then((response) => {
      this.setState({
        userId: response
      });
    });
    navigator.geolocation.getCurrentPosition(
      (position) => {
        console.warn("latitude geolocation", position.coords.latitude)
        console.warn("Loingitude geolocation", position.coords.longitude)
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null,
        });
      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: true, timeout: 30000 },
    );
  }



  closeDrawer = () => {
  this.drawer._root.close()
  };
  
  openDrawer = () => {
  this.drawer._root.open()
  };

  _getProvinsi = () => {
    axios
        .get('https://pokentik.com/api/getProvinsi')
        .then((response) => {
          var newProvinsiOptions = response.data;
          this.setState({
            provinsiOptions: newProvinsiOptions
          });
        })
        .catch((error) =>{
          console.warn("Error notification", error.response);
        })
  }

  _getProvinsiFirst = () => {
    axios
        .get('https://pokentik.com/api/getProvinsi')
        .then((response) => {
          var newProvinsiOptions = response.data;
          console.warn("first provinsi", response.data)
          this.setState({
            provinsiOptions: newProvinsiOptions,
            provinsi: newProvinsiOptions[0]
          });
          return response.data;
        })
        .catch((error) =>{
          console.warn("Error notification", error.response);
        })
  }

  _getKab_Kota = (provinsi) => {
    console.warn("Ini provinsi", provinsi)
    axios
        .get('https://pokentik.com/api/getKabupatenKota/'+ provinsi)
        .then((response) => {
          var newKabKotaOptions = response.data;
          this.setState({
            kab_kotaOptions: newKabKotaOptions
          });
        })
        .catch((error) =>{
          console.warn("Error notification", error.response);
        })
  }

  _getKab_KotaFirst = (provinsi) => {
    axios
        .get('https://pokentik.com/api/getKabupatenKota/'+ provinsi)
        .then((response) => {
          var newKabKotaOptions = response.data;
          this.setState({
            kab_kotaOptions: newKabKotaOptions,
            kab_kota:newKabKotaOptions[0]
          });
        })
        .catch((error) =>{
          console.warn("Error notification", error.response);
        })
  }

  _getKecamatan = (kab_kota) => {
    axios
        .get('https://pokentik.com/api/getKecamatan/'+ kab_kota)
        .then((response) => {
          var newKecamatanOptions = response.data;
          this.setState({
            kecamatanOptions: newKecamatanOptions
          });
        })
        .catch((error) =>{
          console.warn("Error notification", error.response);
        })
  }

  _getKecamatanFirst = (kab_kota) => {
    axios
        .get('https://pokentik.com/api/getKecamatan/'+ kab_kota)
        .then((response) => {
          var newKecamatanOptions = response.data;
          this.setState({
            kecamatanOptions: newKecamatanOptions,
            kecamatan: newKecamatanOptions[0]
          });
        })
        .catch((error) =>{
          console.warn("Error notification", error.response);
        })
  }

  _getKelurahan = (kecamatan) => {
    axios
        .get('https://pokentik.com/api/getKelurahan/'+ kecamatan)
        .then((response) => {
          // console.warn("Ini notifikasi",response.data);
          var newKelurahanOptions = response.data;
          this.setState({
            kelurahanOptions: newKelurahanOptions
          });
        })
        .catch((error) =>{
          console.warn("Error notification", error.response);
        })
  }

  _getKelurahanFirst = (kecamatan) => {
    axios
        .get('https://pokentik.com/api/getKelurahan/'+ kecamatan)
        .then((response) => {
          // console.warn("Ini notifikasi",response.data);
          var newKelurahanOptions = response.data;
          this.setState({
            kelurahanOptions: newKelurahanOptions,
            kelurahan:newKelurahanOptions[0]
          });
        })
        .catch((error) =>{
          console.warn("Error notification", error.response);
        })
  }

  _goToForm = () => {
    this.setState({
      mapView:false
    });
  }

  _next = () => {
    let nextForm = this.state.formField + 1;
    this.setState({
      formField:nextForm
    });
  }

  _previous = () => {
    if(this.state.formField > 1){
      let previousForm = this.state.formField - 1;
      this.setState({
        formField:previousForm
      });
    }
    else{
      this.setState({
        mapView:true
      });
    }
  }

  _onRegionChange = (region) => {
    console.log('onRegionChange', region);
  };

  _onRegionChangeComplete = (region) => {
    console.log('onRegionChangeComplete', region);
    this.setState({ latitude:region.latitude,
                    longitude:region.longitude,
                    latitudeDelta:region.latitudeDelta,
                    longitudeDelta:region.longitudeDelta 
    });
  };

  _appendImage = (image) => {
    const data = new FormData();
    // data.append('name', 'fotoAktivitas'); // you can append anyone.
    // data.append('file', {
    //   uri: image.uri,
    //   type: 'image/jpeg', // or photo.type
    //   name: 'fotoAktivitas.jpg'
    // });
    data.append('file', image)
    return data;
  }

  _submit = (activity) => {
    
    // axios({
    // method: 'post',
    // url: 'https://pokentik.com/api/larvaeStore',
    // data: activity,
    // config: { headers: {'Content-Type': 'multipart/form-data' }}
    // })
    //     // .post('https://pokentik.com/api/larvaeStore', activity)
    //     .then((response) => {
    //       console.warn("Ini larvae store",response.data);
    //       this.setState({
    //         submitted:true
    //       });     
    //     })
    //     .catch((error) =>{
    //       console.warn("Error activity", error.response);
    //     })

    fetch('https://pokentik.com/api/larvaeStore', {  
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data'
      },
      method: 'POST',
      body: activity
    })
    .then(response => {
      console.warn("Ini larvae store",response);
      this.setState({isLoading:false})
      if(response && (response.status!=404 || response.message=="success")){
        NavigationService.navigate('Info', {message:"Add Aktivitas Berhasil",header:" ",title:"Success",buttonText:"LANJUT KE HOME"});  
      }
      else{
        ToastAndroid.show("Add aktivitas gagal, pastikan semua isian sudah diisi dan gambar sudah di upload.", ToastAndroid.SHORT);
      }      
    })
    .catch(error => {
      this.setState({isLoading:false})
      console.warn("Error", error);
       ToastAndroid.show("Add aktivitas gagal, pastikan semua isian sudah diisi dan gambar sudah di upload.", ToastAndroid.SHORT);
    });
  
  }

  _pickImageHandler = (imageStateName, loadingStateName) => {
    console.warn("nama state image", imageStateName);
    this.setState({
            [loadingStateName]: true,
    });
    const options = {
      title: 'Upload gambar',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.warn('Response = ', response);
      
      if (response.didCancel) {
        console.warn('User cancelled image picker');
        this.setState({
            [loadingStateName]: false,
        });
      } else if (response.error) {
          console.warn('ImagePicker Error: ', response.error);
          this.setState({
            [loadingStateName]: false,
          });
      } else if (response.customButton) {
          console.warn('User tapped custom button: ', response.customButton);
          this.setState({
            [loadingStateName]: false,
          });
      } else {
          // const source = { uri: response.uri };
          const source = response;
          console.warn('Source = ', source);
          this.setState({
            [imageStateName]: source,
            [loadingStateName]: false,
          });
      }
    });
  }

  _resetImage = () => {
    this.setState({
      imageSebelum: null
    });
  }

  _resetImageHandler = () =>{
    this._resetImage();
  }

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = (date) => {
    //console.warn('A date has been picked: ', date);
    let observationDate = moment(date).format('YYYY-MM-DD')
    this.setState({
      tanggalObservasi:observationDate
    })
    this._hideDateTimePicker();
  };

  _submitHandler = (imageSebelum, imageSesudah) => {
    
    // let activity = {};
    // let imageDataSesudah = null;
    // let imageDataSebelum = null;
    // if(imageSebelum){
    //   imageDataSebelum = this._appendImage(imageSebelum);
    // }
    // if(imageSesudah){
    //   let imageDataSesudah = this._appendImage(imageSesudah);
    // }
    
    // activity.user_id = this.state.userId;
    // activity.address = this.state.alamat;
    // activity.country = this.state.negara;
    // activity.province = this.state.provinsi;
    // activity.city = this.state.kab_kota;
    // activity.district = this.state.kecamatan;
    // activity.village = this.state.kelurahan;
    // activity.rt = this.state.rt;
    // activity.rw = this.state.rw;
    // activity.container_category = this.state.wadah;
    // activity.place_category = this.state.tempat;
    // activity.place_type = this.state.tipeTempat;
    // activity.any_larvae = this.state.adaJentik;
    // activity.observing_date = this.state.tanggalObservasi;
    // activity.latitude = this.state.latitude;
    // activity.longitude = this.state.longitude;
    // activity.num_container = this.state.jumlahWadah;
    // if(imageDataSebelum){
    //   activity.foto_before = imageDataSebelum;  
    // }
    // if (imageDataSesudah){
    //   activity.foto_after = imageDataSesudah;
    // }

    // activity.foto_before = imageSebelum;
    // activity.foto_after = imageSesudah;
    this.setState({isLoading:true})
    var thisEntity = this;
    let activity = new FormData();

    activity.append("observing_date", this.state.tanggalObservasi);
    activity.append("user_id", this.state.userId);
    activity.append("address", this.state.alamat);
    activity.append("country", this.state.negara);
    activity.append("province", this.state.provinsi.name);
    activity.append("city", this.state.kab_kota.name);
    activity.append("district", this.state.kecamatan.name);
    activity.append("village", this.state.kelurahan.name);
    activity.append("rt", this.state.rt);
    activity.append("rw", this.state.rw);
    activity.append("container_category", this.state.wadah);
    activity.append("place_category", this.state.tempat);
    activity.append("place_type", this.state.tipeTempat);
    activity.append("num_container", this.state.jumlahWadah);
    activity.append("latitude", this.state.latitude);
    activity.append("longitude", this.state.longitude);
    activity.append("any_larvae", this.state.adaJentik);
    activity.append("mosquito_type", this.state.jentik);
    if(imageSebelum){
      activity.append('foto_before', {
        uri: imageSebelum.uri,
        type: 'image/jpeg', // or photo.type
        name: 'user' + thisEntity.state.userId + imageSebelum.fileName
      });
    }
    if(imageSesudah){
      activity.append('foto_after', {
        uri: imageSesudah.uri,
        type: 'image/jpeg', // or photo.type
        name: 'user' + thisEntity.state.userId + imageSesudah.fileName
      });
    }
    // activity.append("foto_before", imageSebelum);
    // activity.append("foto_after", imageSesudah);
    
    
    console.warn("activity baru", activity);

    this._submit(activity);
  
  }

  _renderJentik = () => {
    var content = null;
    if(this.state.jentik!=''){
      if(this.state.jentik=='Aedes Aegypti'){
         content = (
          <View style={styles.descJentik}>
            <Text style={styles.descJentikTitle}>
              Aedes Aegypti, ciri-cirinya :
            </Text>
            <Text style={styles.descJentikExplanation}>
              - Bentuk siphon besar dan pendek terdapat pada abdomen terakhir
            </Text>
            <Text style={styles.descJentikExplanation}>
              - Bentuk comb seperti sisir
            </Text>
            <Text style={styles.descJentikExplanation}>
              - Pada bagian thorax terdapat stroot spine
            </Text>
            <Text style={styles.descJentikExplanation}>
              - Jika diberikan cahaya senter jentik berkumpul mengikuti cahaya tersebut
            </Text>
          </View>
          );
       }
       else if(this.state.jentik=='Anopheles'){
         content = (
          <View style={styles.descJentik}>
            <Text style={styles.descJentikTitle}>
              Anopheles, ciri-cirinya :
            </Text>
            <Text style={styles.descJentikExplanation}>
              - Bentuk siphon seperti tanduk 
            </Text>
            <Text style={styles.descJentikExplanation}>
              - Jentik menempel pada akar tumbuhan air 
            </Text>
            <Text style={styles.descJentikExplanation}>
              - Thoraxs terdapat stoot spine 
            </Text>
          </View>
          );
       }
       else if(this.state.jentik=='Culex sp'){
        content = (
          <View style={styles.descJentik}>
            <Text style={styles.descJentikTitle}>
              Culex sp, ciri-cirinya :
            </Text>
            <Text style={styles.descJentikExplanation}>
              - Siphon langsing dan kecil pada abdomen terakhir  
            </Text>
            <Text style={styles.descJentikExplanation}>
              - Bentuk comb tidak beraturan
            </Text>
            <Text style={styles.descJentikExplanation}>
              - Membentuk sudut di tumbuhan air (menggantung)
            </Text>
          </View>
          );
       }
    }
    return content;
  }

  render() {
    const homePlace = { description: 'Home', geometry: { location: { lat: 48.8152937, lng: 2.4597668 } }};
    const workPlace = { description: 'Work', geometry: { location: { lat: 48.8496818, lng: 2.2940881 } }};
    var loading = (<Spinner color="#18B5AE" />);

    const GooglePlacesInput = () => {
      return (
        <GooglePlacesAutocomplete
          placeholder='Cari tempat...'
          minLength={2} // minimum length of text to search
          autoFocus={false}
          returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
          listViewDisplayed={false}    // true/false/undefined
          fetchDetails={true}
          ref={(instance) => { this.GooglePlacesRef = instance }}
          renderDescription={row => row.description} // custom description render
          onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
            console.log("Ini data", data);
            console.log("Ini details", details);
            this.setState({
              latitude:details.geometry.location.lat,
              longitude:details.geometry.location.lng,
            })
            
            console.warn("Latitude longitude ", this.state.latitude, this.state.longitude)
          }}
          
          getDefaultValue={() => ''}
          
          query={{
            // available options: https://developers.google.com/places/web-service/autocomplete
            key: 'AIzaSyDka3w5MedbhBA0Ole1lc6qPapVP5jVXJw',
            language: 'id', // language of the results
            //types: '(regions)', // default: 'geocode'
            components: 'country:id',
            // radius: '15000', //15 km
            // strictbounds: true,
          }}
          
          styles={{
            container:{
              backgroundColor:'#FFFFFF',
              border:'none',
            },
            textInputContainer: {
              width: '100%',
              backgroundColor:'#FFFFFF',
              borderTopWidth: 0,
              borderBottomWidth:0
            },
            description: {
              fontWeight: 'bold'
            },
            predefinedPlacesDescription: {
              color: '#1faadb'
            },
            listView:{
              backgroundColor:'#FFFFFF',
              zIndex:999,
            },
            powered:{
              display:'none'
            }
          }}
          currentLocation={false} // Will add a 'Current location' button at the top of the predefined places list
          currentLocationLabel="Tempat saat ini"
          nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
          // GoogleReverseGeocodingQuery={{
          //   // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
          // }}
          // GooglePlacesSearchQuery={{
          //   // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
          //   rankby: 'distance',
          //   types: 'food'
          // }}
          //filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
          debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.          
          renderLeftButton={()  => <View style={{paddingTop:10, paddingLeft:10}}><Icon active name='ios-search' /></View>}
        />
      );
    } 

    var thisEntity =this;
    var initialLatitude = this.state.latitude;
    var initialLongitude = this.state.longitude;

    var firstSetForm = (
      <View>
        <View>
          <ListItem noIndent style={{marginTop:20, marginBottom:20, borderBottom:1}}>
              <Left>
                  <Text>Tanggal Aktivitas</Text>
              </Left>
              <Body>
                <Button bordered onPress={this._showDateTimePicker}>
                  <Text>{this.state.tanggalObservasi}</Text>
                </Button>
              </Body>
              <DateTimePicker
                isVisible={this.state.isDateTimePickerVisible}
                onConfirm={this._handleDatePicked}
                onCancel={this._hideDateTimePicker}
                datePickerModeAndroid="spinner"
              />
          </ListItem>
          <Text style={styles.textQuestion}>Alamat</Text>
          <UnderlineTextForm placeholder="Alamat" onChangeText={(alamat) => this.setState({alamat})} value={thisEntity.state.alamat} />
          {this.state.alamat==''?
              <Text style={{color:'red',  marginTop:10, marginBottom:10, fontSize:10,}}>
                Alamat wajib diisi  
              </Text>
              : 
            null}
          <Text style={styles.textQuestion}>Provinsi</Text>
          <UnderlinePickerActivity placeholder="Provinsi" onValueChange={(provinsi) => {this.setState({provinsi}, ()=> thisEntity._getKab_Kota(provinsi.id)); }} selected={thisEntity.state.provinsi} options={thisEntity.state.provinsiOptions} />
          <Text style={styles.textQuestion}>Kabupaten/Kota</Text>
          <UnderlinePickerActivity placeholder="Kabupaten/Kota" onValueChange={(kab_kota) => {this.setState({kab_kota}, () => thisEntity._getKecamatan(kab_kota.id)); }} selected={thisEntity.state.kab_kota} options={thisEntity.state.kab_kotaOptions} />
          <Text style={styles.textQuestion}>Kecamatan</Text>
          <UnderlinePickerActivity placeholder="Kecamatan" onValueChange={(kecamatan) => {this.setState({kecamatan}, () => thisEntity._getKelurahan(kecamatan.id));}} selected={thisEntity.state.kecamatan} options={thisEntity.state.kecamatanOptions} />            
          <Text style={styles.textQuestion}>Kelurahan</Text>
          <UnderlinePickerActivity placeholder="Kelurahan" onValueChange={(kelurahan) => {this.setState({kelurahan});}} selected={thisEntity.state.kelurahan} options={thisEntity.state.kelurahanOptions} />
          <Text style={styles.textQuestion}>RT</Text>
          <UnderlineTextForm placeholder="RT" onChangeText={(rt) => this.setState({rt})} value={thisEntity.state.rt} />
          {this.state.rt==''?
              <Text style={{color:'red',  marginTop:10, marginBottom:10, fontSize:10,}}>
                RT wajib diisi  
              </Text>
              : 
            null}
          <Text style={styles.textQuestion}>RW</Text>
          <UnderlineTextForm placeholder="RW" onChangeText={(rw) => this.setState({rw})} value={thisEntity.state.rw} />
          {this.state.rw==''?
              <Text style={{color:'red',  marginTop:10, marginBottom:10, fontSize:10,}}>
                RW wajib diisi  
              </Text>
              : 
            null}
        </View>
        <View style={styles.buttonContainer}>
            <View style={styles.buttonLeft}>
            <Button success style={styles.formButton} onPress={this._previous}>
                <Text>Previous</Text>
            </Button>
          </View>
          <View style={styles.buttonRight}>
              <Button success style={styles.formButton} onPress={this._next}>
                <Text>Next</Text>
            </Button>
          </View>
        </View>
    </View>
    );

    var secondSetForm=(
      <View>
        <View>
          <Text style={styles.textQuestion}>Wadah</Text>
          <UnderlinePicker placeholder="Wadah" options={this.state.wadahOptions} onValueChange={(wadah) => this.setState({wadah})}  selected={this.state.wadah} />
          <Text style={styles.textQuestion}>Jumlah Wadah</Text>
          <UnderlineTextForm placeholder="Jumlah Wadah" keyboardType="phone-pad" onChangeText={(jumlahWadah) => this.setState({jumlahWadah})} value={thisEntity.state.jumlahWadah} />
          {this.state.jumlahWadah==''?
              <Text style={{color:'red',  marginTop:10, marginBottom:10, fontSize:10,}}>
                Jumlah wadah wajib diisi  
              </Text>
              : 
            null}
          <Text style={styles.textQuestion}>Tempat</Text>
          <UnderlinePicker placeholder="Tempat" options={this.state.tempatOptions} onValueChange={(tempat) => this.setState({tempat})} selected={this.state.tempat} />
          <Text style={styles.textQuestion}>Tipe Tempat</Text>
          <UnderlinePicker placeholder="Tipe Tempat" options={this.state.tipeTempatOptions} onValueChange={(tipeTempat) => this.setState({tipeTempat})} selected={this.state.tipeTempat} />
          <Text style={styles.textQuestion}>Ada Jentik</Text>
          <UnderlinePicker placeholder="Ada Jentik?" options={this.state.adaJentikOptions} onValueChange={(adaJentik) => this.setState({adaJentik})} selected={this.state.adaJentik} />
        </View>
        <View style={styles.buttonContainer}>
            <View style={styles.buttonLeft}>
            <Button success style={styles.formButton} onPress={this._previous}>
                <Text>Previous</Text>
            </Button>
          </View>
          <View style={styles.buttonRight}>
              <Button success style={styles.formButton} onPress={this._next}>
                <Text>Next</Text>
            </Button>
          </View>
        </View>
    </View>
    );

    var thirdSetForm=(
      <View>
        <View>

          {/*Tombol image sebelum*/}
          <Button success style={styles.formButtonUpload} onPress={()=>{this._pickImageHandler("imageSebelum","imageLoadingSebelum")}} >
            <Text>
              Upload foto sebelum
            </Text>
          </Button>
          {//Nama gambar, dan loading
            this.state.imageLoadingSebelum ? 
              loading
              : 
              <UnderlineTextForm disabled={true} 
              placeholder="Upload foto sebelum . . ." 
              value={this.state.imageSebelum&&this.state.imageSebelum.uri ? this.state.imageSebelum.uri : null} /> 
          }
          {this.state.imageSebelum==null&&this.state.imageLoadingSebelum==false ?
              <Text style={{color:'red',  marginTop:10, marginBottom:10, fontSize:10,}}>
                Gambar sebelum wajib di upload  
              </Text>
              : 
            null
            }
            {this.state.imageSebelum&&this.state.imageSebelum.uri?
              <Image
              style={{marginTop:20, width:100,height:100,resizeMode:'contain'}}
              source={{uri: this.state.imageSebelum.uri}}
            /> :
            null
             }
          
          {/*Button hapus gambar sebelum disini nanti*/}

          {/*Tombol image sesudah*/}
          <Button success style={styles.formButtonUpload} onPress={()=>{this._pickImageHandler("imageSesudah", "imageLoadingSesudah")}}>
            <Text>
              Upload foto sesudah
            </Text>
          </Button>
          {//Nama gambar, dan loading
            this.state.imageLoadingSesudah ? 
              loading 
              : 
            <UnderlineTextForm disabled={true} 
            placeholder="Upload foto sesudah . . ." 
            value={this.state.imageSesudah&&this.state.imageSesudah.uri ? this.state.imageSesudah.uri : null} />
          }
           {this.state.imageSesudah==null&&this.state.imageLoadingSesudah==false?
              <Text style={{color:'red',  marginTop:10, marginBottom:10, fontSize:10,}}>
                Gambar sesudah wajib di upload  
              </Text>
              : 
            null}
            {this.state.imageSesudah&&this.state.imageSesudah.uri?
              <Image
              style={{marginTop:20, width:100,height:100,resizeMode:'contain'}}
              source={{uri: this.state.imageSesudah.uri}}
            /> :
            null
             }
         {/*Button hapus gambar sesudah disini nanti*/}

        </View>
        <View style={styles.buttonContainer}>
            <View style={styles.buttonLeft}>
            <Button success style={styles.formButton} onPress={this._previous}>
                <Text>Previous</Text>
            </Button>
          </View>
          <View style={styles.buttonRight}>
              <Button success style={styles.formButton} onPress={this._next}>
                <Text>Next</Text>
            </Button>
          </View>
        </View>
    </View>
    );

    var fourthSetForm=(
      <View>
        <View>
        <Text style={styles.textQuestion}>Jenis jentik</Text>
          <UnderlinePicker 
          placeholder="Jenis Jentik" 
          options={this.state.jentikOptions} 
          onValueChange={(jentik) => {this.setState({jentik}); console.warn("Ini jentik", jentik);}} 
          selected={this.state.jentik} />
        </View>
        <Text style={{color:'grey',  marginTop:10, marginBottom:10, fontSize:10,}}>
          (Jenis jentik tidak wajib diisi, tapi boleh diisi sebagai info tambahan)  
        </Text>
       {this._renderJentik()}
        <View style={styles.buttonContainer}>
            <View style={styles.buttonLeft}>
            <Button success style={styles.formButton} onPress={this._previous}>
                <Text>Previous</Text>
            </Button>
          </View>
          <View style={styles.buttonRight}>
              <Button success style={styles.formButton} onPress={()=>{this._submitHandler(this.state.imageSebelum, this.state.imageSesudah)}}>
                <Text>Submit</Text>
            </Button>
          </View>
        </View>
      </View>
    )

    return (
      <Drawer
          ref={(ref) => { this.drawer = ref; }}
          content={<SideBar  />}
          onClose={() => this.closeDrawer()} >
          <MainHeader openDrawer={() => this.openDrawer()} title="Aktivitas" back={() => { NavigationService.goBack();}} />
          {this.state.mapView ? 
            <View style={{position: 'absolute', zIndex:100, top: 0, left:0, right: 0, alignItems: 'stretch', justifyContent: 'center', backgroundColor: 'transparent'}}>
             <MainHeader openDrawer={() => this.openDrawer()} title="Aktivitas" back={() => { NavigationService.goBack();}} />
            </View> 
            : 
            null
          }
          { this.state.mapView && this.state.latitude && this.state.longitude ? 
            <MapView style={styles.map}
              
              region={{
                latitude: this.state.latitude,
                longitude: this.state.longitude,
                latitudeDelta: this.state.latitudeDelta,
                longitudeDelta: this.state.longitudeDelta,                
              }}
              onRegionChange={this._onRegionChange}
              onRegionChangeComplete={this._onRegionChangeComplete}
              onMapReady={() => {
                  thisEntity.setState({ regionSet: true });
              }}


              >
              {//   <Marker
              //     coordinate={{
              //   latitude: 37.78825,
              //   longitude: -122.4324,
              //   latitudeDelta: 0.0922,
              //   longitudeDelta: 0.0421,
              // }}
              //     title="Marker 1"
              //     description="Marker 1"
              //   />
              }
             

              </MapView>
               
            :
              <View style={styles.info}>
                <Text style={styles.infoText}>
                  Silakan isi data aktivitas di bawah
                </Text>
              </View>
          }
          
        {this.state.mapView ? null : 
          <Container style={{ flex: 1, padding:10, justifyContent: 'center' }}>
            <Content>
                {this.state.isLoading? loading : (this.state.formField==1 ? firstSetForm : (this.state.formField==2 ? secondSetForm : (this.state.formField==3 ? thirdSetForm : fourthSetForm)))}
              </Content>
         </Container>
      }
      {this.state.mapView ? 
        <View pointerEvents="none" style={{position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, alignItems: 'center', justifyContent: 'center', backgroundColor: 'transparent'}}>
         {/*<Icon name="ios-close-circle-outline" style={{fontSize: 30, color: 'red'}} />*/}
         <Image
           style={{
            width:50,
            height:50,
           }}
           source={require('../../../assets/img/red-circle.png')}
         />
         
        </View> 
        : 
        null}
        {this.state.mapView ? 
        <View style={{position: 'absolute', bottom: 10, right: 20, alignItems: 'center', justifyContent: 'center', backgroundColor: 'transparent'}}>
         <Button success style={styles.formButton} onPress={this._goToForm}>
            <Text>Next</Text>
          </Button>
        </View> 
        : 
        null}
        {this.state.mapView ? 
            <View style={{position: 'absolute', top: 80, left:10, right: 10, alignItems: 'center', justifyContent: 'center', backgroundColor: 'transparent'}}>
             {/*<Item style={{backgroundColor:'#FFFFFF', paddingLeft:20}}>
                <Icon active name='ios-search' />
                <Input placeholder='Cari tempat ...' style={{backgroundColor:'#FFFFFF'}}/>
              </Item>
            */}
              {GooglePlacesInput()}
            </View> 
            : 
            null
          }
      </Drawer>
    );
  }
}

Drawer.defaultProps.styles.mainOverlay.elevation = 2;