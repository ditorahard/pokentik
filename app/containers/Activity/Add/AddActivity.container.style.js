import {StyleSheet} from 'react-native';
// import theme from '../../styles/theme.style';
// import {headingText, textInput} from '../../styles/common.style';

export default StyleSheet.create({
	home:{
		flex: 1, 
		//alignItems: 'center', 
		justifyContent: 'center',
		paddingTop:60,
		marginBottom:10,
	},
	info:{
		// borderBottomWidth:1,
		// borderColor:'grey',
		padding:10,
		backgroundColor:"#FFFFFF",
		elevation:4,
		shadowOffset: { width: 5, height: 5 },
		shadowColor: "grey",
		shadowOpacity: 0.5,
		shadowRadius: 10,
		marginBottom:5,
	},
	infoText:{
		fontWeight:"400",
		color:'grey'
	},
	whitetext:{
		color:'#FFFFFF',
		fontSize:20,
		fontWeight:'700',
	},
	ajardiaLogo:{
		 flex: 1,
	     resizeMode: 'contain'
	},
	buttonContainer:{
		flexDirection:'row', 
		flexWrap:'wrap',
		paddingTop:20,
	},
	buttonLeft:{
		marginRight:'auto',
	},
	buttonRight:{
		marginLeft:'auto',
	},
	formButton:{
		backgroundColor:"#18B5AE",
	},
	formButtonUpload:{
		backgroundColor:"#18B5AE",
		marginTop:10,
	},
	map: {
	    position: 'absolute',
	    top: 0,
	    left: 0,
	    right: 0,
	    bottom: 0,
	},
	 map: {
	    ...StyleSheet.absoluteFillObject,
	  },
	descJentik: {
		padding:20
	},
	descJentikTitle:{
		marginBottom:10,
		fontWeight:'700'
	},
	descJentikExplanation:{
		marginBottom:10,
		fontSize:14,
	},
	textQuestion:{
		fontSize:15,
		marginTop:20,
		marginBottom:3
	}
	// logoContainer:{
	// 	height:160,
	// },
});