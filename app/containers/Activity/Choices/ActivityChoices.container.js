import React, {Component} from 'react';
import { View, Image, Linking, ToastAndroid } from 'react-native';
import { Container, Content, Text, Body, Drawer } from "native-base";
import MainHeader from '../../../components/Header/MainHeader.component';
import SideBar from '../../../components/Sidebar/Sidebar.component';
import NotificationListItem from '../../../components/ListItem/Notifications/NotificationListItem.component';
import styles from './ActivityChoices.container.style';
import NavigationService from '../../../routes/NavigationService';

export default class ActivityChoices extends React.Component {
  
  closeDrawer = () => {
  this.drawer._root.close()
  };
  
  openDrawer = () => {
  this.drawer._root.open()
  };

  render() {

    // const { navigation } = this.props;
    // const grupKelurahan = navigation.getParam('grupKelurahan', null);
    // const grupKecamatan = navigation.getParam('grupKecamatan', null);

    return (
      <Drawer
          ref={(ref) => { this.drawer = ref; }}
          content={<SideBar  />}
          onClose={() => this.closeDrawer()} >
        <MainHeader openDrawer={() => this.openDrawer()} title="Aktivitas" />
        <Container style={{ flex: 1, padding:10, alignItems:"flex-start" }}>
          <Content>
              <NotificationListItem title={"Add Aktivitas"} content={"Tambah aktivitas baru"} action={()=>{NavigationService.navigate('AddActivity')}} />
              <NotificationListItem title={"Daftar Aktivitas"} content={"Daftar aktivitas"} action={()=>{NavigationService.navigate('ActivityList')}} /> 
          </Content>
        </Container>
      </Drawer>
    );
  }
}

Drawer.defaultProps.styles.mainOverlay.elevation = 2;
