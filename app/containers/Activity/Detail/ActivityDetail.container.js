import React, {Component} from 'react';
import { View, TouchableOpacity, Linking, Image } from 'react-native';
import { Header, Container, Content, Text, Body, Drawer, Tab, Tabs, Spinner, ScrollableTab } from "native-base";
import PropTypes from 'prop-types';
import style from './ActivityDetail.container.style';
import { StackNavigator } from "react-navigation";
import MainHeader from '../../../components/Header/MainHeader.component';
import SideBar from '../../../components/Sidebar/Sidebar.component';
import ProfileDetail from '../../../components/Profile/Detail/ProfileDetail.component';
import NavigationService from '../../../routes/NavigationService';
import deviceStorage from '../../../services/deviceStorage';
import axios from 'axios';
import ActivityHeader from '../../../components/ActivityHeader/ActivityHeader.component';
import moment from 'moment';

export default class ActivityDetail extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
        	token:'',
        	account:{},
        	activity:{},
        	wadahOptions:[
            {
              name:"Bak mandi", id:"1"
            }, 
            {
              name:"Bak wc", id:"2"
            },
            {
              name:"Tempayan", id:"3"
            },
            {
              name:"Ember", id:"4"
            },
            {
              name:"Dispenser", id:"5"
            },
            {
              name:"Pot bunga", id:"6"
            },
            {
              name:"Kolam", id:"7"
            },
            {
              name:"Ban bekas", id:"8"
            },
            {
              name:"Botol bekas", id:"9"
            },
            {
              name:"Lain-lain", id:"1"
            },
          ],
          tempatOptions:[
            {
              name:"Tempat tinggal", id:"1"
            },
            {
              name:"Tempat Umum", id:"2"
            },
          ],
          tipeTempatOptions:[
            {
              name:"Indoor", id:"1",
            },
            {
              name:"Outdoor", id:"2",
            },
          ],
          adaJentikOptions:[
            {
              name:"Ya", id:"1",
            },
            {
              name:"Tidak", id:"2",
            },
          ],
        }
    }

	componentDidMount(){
		const { navigation } = this.props;
		const idActivity = navigation.getParam('idActivity', 'NO-ID');
		// console.warn("id activity", idActivity); 
    	if(this.state.token==''){;			
			deviceStorage.takeItem('token').then((response) => {
				let newToken = response;
				this.setState({
					token: newToken 
				});
				// console.warn('toke n home', newToken);
				let getUserInput = {}
				getUserInput.token = newToken;
				this.getUser(getUserInput);
				// console.warn('get user input', getUserInput);
			});	
		}
		else{
			var newToken = this.state.token;
			// console.warn('toke n home established token', newToken);
			let getUserInput = {}
			getUserInput.token = newToken;
			this.getUser(getUserInput);
			// console.warn('get user input', getUserInput);
		}
		if(idActivity){
			this.getActivityDetail(idActivity);
		}
    }

    getUser = (token) => {
    	axios
	      .post('https://pokentik.com/api/getUser', token)
	      .then((response) => {
	        // console.warn("Ini user",response.data.data);
	        var newAccount = response.data.data;
	        this.setState({
	        	account: newAccount
	        });
	      })
	      .catch((error) =>{
	        // console.warn("Error user", error.response);
	      })
    }

    getActivityDetail = (idActivity) => {
    	axios
	      .post('https://pokentik.com/api/getSingleUserLarvaeNest', {larvaeNestId:idActivity})
	      .then((response) => {
	        console.warn("Ini activity detail",response.data.data);
	        var newActivity = response.data.data;
	        this.setState({
	        	activity: newActivity
	        });
	      })
	      .catch((error) =>{
	        // console.warn("Error activity", error);
	      })
    }
	

	closeDrawer = () => {
	    this.drawer._root.close()
	};
	
	openDrawer = () => {
	    this.drawer._root.open()
	};

	render() {
		var thisEntity = this;
		var sex = null;
		if(thisEntity.state.account.sex){
			sex = JSON.parse(JSON.stringify(thisEntity.state.account.sex))
		}
		var loading = (<Spinner color="#18B5AE" />);
		var activityDate = moment(this.state.activity.created_at).format('YYYY-MM-DD');
	    return (
	      <Drawer
          ref={(ref) => { this.drawer = ref; }}
          content={<SideBar  />}
          onClose={() => this.closeDrawer()} >
	          <MainHeader openDrawer={() => this.openDrawer()} title="Detail Aktivitas" back={() => { NavigationService.goBack();}} />
	          <ActivityHeader image={this.state.activity.join_image} title={this.state.activity.city ? this.state.activity.city : "Loading ..."} />
			  <Header hasTabs style={style.tabHeader} androidStatusBarColor="#18B5AE"/>
			  <Tabs>
		          <Tab heading="LOKASI" tabStyle={style.tabTitle} textStyle={style.tabText} activeTabStyle={style.activeTabTitle} activeTextStyle={style.activeTabText}>
			            <Container>
							<Content contentContainerStyle={style.profileInfo}>
								<View style={style.profileInfoSegment}>
									<Text style={style.infoHeader}>Tanggal Aktivitas</Text>
									<Text style={style.infoContent}>{this.state.activity.created_at!=null ? activityDate  : "Loading ..."}</Text>
								</View>
								<View style={style.profileInfoSegment}>
									<Text style={style.infoHeader}>Provinsi</Text>
									<Text style={style.infoContent}>{this.state.activity.province ? this.state.activity.province  : "Loading ..."}</Text>
								</View>
								<View style={style.profileInfoSegment}>
									<Text style={style.infoHeader}>Kota</Text>
									<Text style={style.infoContent}>{this.state.activity.city ? this.state.activity.city  : "Loading ..."}</Text>
								</View>
								<View style={style.profileInfoSegment}>
									<Text style={style.infoHeader}>Kelurahan</Text>
									<Text style={style.infoContent}>{this.state.activity.district ? this.state.activity.district  : "Loading ..."}</Text>
								</View>
								<View style={style.profileInfoSegment}>
									<Text style={style.infoHeader}>Kecamatan</Text>
									<Text style={style.infoContent}>{this.state.activity.village ? this.state.activity.village  : "Loading ..."}</Text>
								</View>
								<View style={style.profileInfoSegment}>
									<Text style={style.infoHeader}>RT</Text>
									<Text style={style.infoContent}>{this.state.activity.rt ? this.state.activity.rt  : "Loading ..."}</Text>
								</View>
								<View style={style.profileInfoSegment}>
									<Text style={style.infoHeader}>RW</Text>
									<Text style={style.infoContent}>{this.state.activity.rw ? this.state.activity.rw  : "Loading ..."}</Text>
								</View>
								<View style={style.profileInfoSegment}>
									<Text style={style.infoHeader}>Alamat</Text>
									<Text style={style.infoContent}>{this.state.activity.address ? this.state.activity.address  : "Loading ..."}</Text>
								</View>
							</Content>
					    </Container>
		          </Tab>
		          <Tab heading="WADAH" tabStyle={style.tabTitle} textStyle={style.tabText} activeTabStyle={style.activeTabTitle} activeTextStyle={style.activeTabText}>
			            <Container>
							<Content contentContainerStyle={style.profileInfo}>
								{/*<View style={style.profileInfoSegment}>
									<Text style={style.infoHeader}>Jumlah Aktifitas</Text>
									<Text style={style.infoContent}>123</Text>
								</View>*/}
								<View style={style.profileInfoSegment}>
									<Text style={style.infoHeader}>Jumlah Wadah</Text>
									<Text style={style.infoContent}>{this.state.activity.num_container ? this.state.activity.num_container  : "Loading ..."}</Text>
								</View>
								<View style={style.profileInfoSegment}>
									<Text style={style.infoHeader}>Jenis Wadah</Text>
									<Text style={style.infoContent}>{this.state.activity.container_category!=null ? this.state.wadahOptions[thisEntity.state.activity.container_category-1].name  : "Loading ..."}</Text>
								</View>
								<View style={style.profileInfoSegment}>
									<Text style={style.infoHeader}>Jenis Tempat</Text>
									<Text style={style.infoContent}>{this.state.activity.place_category!=null ? this.state.tempatOptions[thisEntity.state.activity.place_category-1].name  : "Loading ..."}</Text>
								</View>
								<View style={style.profileInfoSegment}>
									<Text style={style.infoHeader}>Tipe Tempat</Text>
									<Text style={style.infoContent}>{this.state.activity.place_type!=null ? this.state.tipeTempatOptions[thisEntity.state.activity.place_type-1].name  : "Loading ..."}</Text>
								</View>
								<View style={style.profileInfoSegment}>
									<Text style={style.infoHeader}>Ada Jentik?</Text>
									<Text style={style.infoContent}>{this.state.activity.any_larvae!=null ? this.state.adaJentikOptions[thisEntity.state.activity.any_larvae-1].name  : "Loading ..."}</Text>
								</View>
							</Content>
					  </Container>
		          </Tab>
		          <Tab heading="FOTO" tabStyle={style.tabTitle} textStyle={style.tabText} activeTabStyle={style.activeTabTitle} activeTextStyle={style.activeTabText}>
			            <Container>
							<Content contentContainerStyle={style.profileInfo}>
								{/*<View style={style.profileInfoSegment}>
									<Text style={style.infoHeader}>Jumlah Aktifitas</Text>
									<Text style={style.infoContent}>123</Text>
								</View>*/}
								<View style={style.profileInfoSegment}>
									<Text style={style.infoHeader}>Foto Sebelum</Text>
									{this.state.activity.before_image_path!=null ?
									<View>	
									<TouchableOpacity onPress={()=>{Linking.openURL('http://pokentik.com/public/foto/' + thisEntity.state.activity.before_image_path)}}>
										<Text style={style.infoContentLink}>
											{this.state.activity.before_image_path}
										</Text>
									</TouchableOpacity>
										<Image
										style={{width:80,height:80,resizeMode:'contain'}}
										  source={{uri: 'https://pokentik.com/public/foto/' + thisEntity.state.activity.before_image_path}}
										/>										
									</View>
									: <Text style={style.infoContent}>Tidak ada foto</Text>}
								</View>
								<View style={style.profileInfoSegment}>
									<Text style={style.infoHeader}>Foto Sesudah</Text>
									{this.state.activity.after_image_path!=null ?
										<View>
										<TouchableOpacity onPress={()=>{Linking.openURL('http://pokentik.com/public/foto/' + thisEntity.state.activity.after_image_path)}} >
											<Text style={style.infoContentLink}>
												 {this.state.activity.after_image_path}  
											</Text>
										</TouchableOpacity>
											<Image
											style={{width:80,height:80,resizeMode:'contain'}} 
											source={{uri: 'https://pokentik.com/public/foto/' + thisEntity.state.activity.after_image_path}}
											/>										
										</View>
									: <Text style={style.infoContent}>Tidak ada foto</Text>}
								</View>
							</Content>
					  </Container>
		          </Tab>
	        </Tabs>
			  
		  </Drawer>
		)
	}
}

Drawer.defaultProps.styles.mainOverlay.elevation = 0;