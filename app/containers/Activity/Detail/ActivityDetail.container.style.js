import {StyleSheet} from 'react-native';
// import theme from '../../styles/theme.style';
// import {headingText, textInput} from '../../styles/common.style';

export default StyleSheet.create({
	menuButton:{
		width:50,
	},
	tabHeader:{
		height:0,
		marginTop:0,
	},
	tabTitle:{
		backgroundColor: "#2CA089",
	},
	tabText:{
		color:'#fafafa',
	},
	activeTabTitle:{
		backgroundColor: "#258773",
	},
	activeTabText:{
		color: "#FFFFFF"
	},
	profileInfo:{
		paddingTop:20,
		alignItems:'center',
	},
	profileInfoSegment:{
		marginBottom:10,
		alignItems:'center',
	},
	infoHeader:{
		fontWeight:'700',
		fontSize:15,
		marginBottom:5,
	},
	infoContent:{
		fontSize:14,
	},
	infoContentLink:{
		fontSize:14,
		color:"#0273CC"
	}

});