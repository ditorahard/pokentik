import React, {Component} from 'react';
import { View, Share, FlatList } from 'react-native';
import { Container, Content, Text, Body, Drawer, Spinner, Button } from "native-base";
import MainHeader from '../../../components/Header/MainHeader.component';
import Subheader from '../../../components/Subheader/Subheader.component';
import SideBar from '../../../components/Sidebar/Sidebar.component';
import ActivityCard from '../../../components/Cards/Activity/ActivityCard.component';
import Searchbar from '../../../components/Searchbar/Searchbar.component';
import styles from './ActivityList.container.style';
import NavigationService from '../../../routes/NavigationService';
import axios from 'axios';
import deviceStorage from '../../../services/deviceStorage';

export default class ActivityList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        	id:'',
        	page:1,
        	limit:10,
        	activity:null,
          refresh:false,
          loadMore:false,
        }
    }
   
   componentDidMount(){
    	if(this.state.id==''){			
			deviceStorage.takeItem('id').then((response) => {
				let newId = response;
				this.setState({
					id: newId 
				});
				//console.warn("id", this.state.id);
				let activityDetail = {};
				// activityDetail.userId = this.state.id;
				activityDetail.userId = this.state.id;
				activityDetail.page = this.state.page;
				activityDetail.limit = this.state.limit;
				this._getActivity(activityDetail);
			});
		}
	}

	_getActivity = (activityDetail) => {
    	axios
	      .post('https://pokentik.com/api/getUserLarvaeNest', activityDetail)
	      .then((response) => {
	        console.warn("Ini activity",response.data.data);
	        var newActivity = response.data.data;
	        this.setState({
	        	activity: newActivity,
            refresh:false,
            loadMore:false,
	        });
	      })
	      .catch((error) =>{
	        console.warn("Error user", error.response);
	      })
    }

  //   _nextActivity = (newLimit) => {
  //   	this.setState({
  //   		limit: newLimit
  //   	})
  //   	var activityDetail = {};
  //   	// activityDetail.userId = this.state.id;
		// activityDetail.userId = this.state.id;
		// activityDetail.page = this.state.page;
		// activityDetail.limit = this.state.limit;
		// this._getActivity(activityDetail);
  //   }

  closeDrawer = () => {
	this.drawer._root.close()
  };
	
  openDrawer = () => {
	this.drawer._root.open()
  };

  _onShareClick(activity, idActivity) {
      Share.share({
        message: 'Pembersihan jentik nyamuk di '+ activity + ' telah berhasil dilakukan! Anda mendapat pesan ini dari aplikasi Pokentik http://pokentik.com/activityImage/' + idActivity,
        url: 'http://www.pokentik.com',
        title: 'Pokentik'
      }, {
        // Android only:
        dialogTitle: 'Share Aktivitas Pokentik',
        // iOS only:
        excludedActivityTypes: [
          'com.apple.UIKit.activity.PostToTwitter'
        ]
      })
    }

    _handleRefresh = () => {
      var thisEntity = this;
      thisEntity.setState({
        refresh:true,
        page:1,
        limit:10,
      },
      () => {
        let activityDetail = {};
        activityDetail.userId = thisEntity.state.id;
        activityDetail.page = thisEntity.state.page;
        activityDetail.limit = thisEntity.state.limit;
        thisEntity._getActivity(activityDetail);
      });
    }

    _handleLoadMore = () => {
      var thisEntity = this;
      thisEntity.setState({
        loadMore:true,
        limit:thisEntity.state.limit + 10,
      },
      () => {
        let activityDetail = {};
        activityDetail.userId = thisEntity.state.id;
        activityDetail.page = thisEntity.state.page;
        activityDetail.limit = thisEntity.state.limit;
        thisEntity._getActivity(activityDetail);
      });
    }




  render() {
  	var thisEntity = this;
  	var activityList = null;
  	var noActivity = (<Text>Anda belum memiliki aktivitas</Text>);
  	var loading = (<Content><Spinner color="#18B5AE" /></Content>);

  	// if (this.state.activity) {
   //          activityList = this.state.activity.map(function (item, i) {
   //          var activityTitle=item.city + ", " + item.district + ", " + item.village 
   //              return (<ActivityCard key={i} 
   //                title={activityTitle} 
   //                subtitle={item.observing_date} 
   //                actionShare={()=>{thisEntity._onShareClick(activityTitle)}}
   //                action={() => { NavigationService.navigate('ActivityDetail', {idActivity:item.id}); }} 
   //                image={item.after_image_path}
   //                />
   //                );
   //      });
   //  }

   if (this.state.activity) {
            activityList = (
              <FlatList
                data={this.state.activity}
                renderItem={({ item }) => (
                  <ActivityCard 
                  title={item.city + ", " + item.district + ", " + item.village} 
                  subtitle={item.observing_date} 
                  actionShare={()=>{thisEntity._onShareClick((item.city + ", " + item.district + ", " + item.village), item.id)}}
                  action={() => { NavigationService.navigate('ActivityDetail', {idActivity:item.id}); }} 
                  image={item.join_image}
                  />
                )}
                keyExtractor={(item, index) => index.toString()}
                refreshing={this.state.refresh}
                onRefresh={this._handleRefresh}
                onEndReached={this._handleLoadMore}
                onEndThreshold={0}

              />
            )
    }

    return (
    	<Drawer
          ref={(ref) => { this.drawer = ref; }}
          content={<SideBar  />}
          onClose={() => this.closeDrawer()} >
	      <MainHeader openDrawer={() => this.openDrawer()} title="Aktivitas" />
	      {/*<Searchbar />*/}
	      <Container style={{ flex: 1, padding:10, justifyContent: 'center' }}>
		    <Content>
		        {this.state.activity ? 
		        	(this.state.activity.length > 0 ? 
		        		activityList 
		        		: 
		        		noActivity ) 
		        	: 
		        loading}
            {this.state.loadMore ? loading : null}
	        </Content>
          <View style={{position: 'absolute', bottom: 10, right: 20, alignItems: 'center', justifyContent: 'center', backgroundColor: 'transparent'}}>
           <Button success style={styles.formButton} onPress={() => { NavigationService.navigate('AddActivity'); }}>
              <Text style={styles.addActivity}>+ Add Activity</Text>
            </Button>
          </View> 
	      </Container>
      </Drawer>
    );
  }
}

Drawer.defaultProps.styles.mainOverlay.elevation = 2;