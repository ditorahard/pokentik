import React, {Component} from 'react';
import { View, Image, ToastAndroid } from 'react-native';
import { Container, Content, Text, Body, Drawer, Button, Spinner } from "native-base";
import MainHeader from '../../components/Header/MainHeader.component';
import SideBar from '../../components/Sidebar/Sidebar.component';
import ActivityHeader from '../../components/ActivityHeader/ActivityHeader.component';
import UnderlineTextForm from '../../components/Forms/Underline/UnderlineTextForm.component';
import styles from './Forgotpassword.container.style';
import axios from 'axios';
import NavigationService from '../../routes/NavigationService';

export default class Forgotpassword extends React.Component {
  constructor(props) {
    super(props);
  
    this.state = {
      email:'',
      isLoading:false,
      emailSubmitted:false,
    };
  }

  _resetPassword = () => {
    console.warn("email", {Email : this.state.email})
    this.setState({isLoading:true})
     axios
        .post('https://pokentik.com/api/resetPassword', {email : this.state.email})
        .then((response) => {
          this.setState({emailSubmitted:true, isLoading:false});    
        })
        .catch((error) =>{
          this.setState({isLoading:false})
          console.warn("Error forgot password", error.response);
          ToastAndroid.show(""+error.response.data.message, ToastAndroid.SHORT);
        })
  }
  
  closeDrawer = () => {
    this.drawer._root.close()
  };
  
  openDrawer = () => {
    this.drawer._root.open()
  };

  render() {
    var thisEntity = this;
    return (
      <Drawer
          ref={(ref) => { this.drawer = ref; }}
          content={<SideBar  />}
          onClose={() => this.closeDrawer()} >
        <MainHeader openDrawer={() => this.openDrawer()} title="Lupa password" />
        <ActivityHeader title="Lupa password?" subtitle="Tulis email anda ..." />
        <Container style={{ flex: 1, padding:10, justifyContent:'center' }}>
        <Content>
        {
          this.state.isLoading ? 
            <Spinner color="#18B5AE" /> 
            : 
            (
              this.state.emailSubmitted ? 
              <View>
                <Text>Email sudah dikirim. Silakan cek email anda</Text>
                <Button block onPress={() => {NavigationService.navigate('Login');}} style={styles.resetPasswordButton}>
                  <Text>
                    Kembali ke Login
                  </Text>
                </Button>
              </View>
              :
              <View>
                <UnderlineTextForm placeholder="Tulis email anda ..." onChangeText={(email) => this.setState({email})} value={thisEntity.state.email} />
                <Button block onPress={thisEntity._resetPassword} style={styles.resetPasswordButton}>
                  <Text>
                    Submit
                  </Text>
                </Button>
              </View>
            )
        }
          
        </Content>
        </Container>
      </Drawer>
    );
  }
}

Drawer.defaultProps.styles.mainOverlay.elevation = 2;