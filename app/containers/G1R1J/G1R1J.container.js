import React, {Component} from 'react';
import { View, Image } from 'react-native';
import { Container, Content, Text, Body, Drawer } from "native-base";
import MainHeader from '../../components/Header/MainHeader.component';
import SideBar from '../../components/Sidebar/Sidebar.component';
import NotificationListItem from '../../components/ListItem/Notifications/NotificationListItem.component';
import styles from './G1R1J.container.style';

export default class G1R1J extends React.Component {
  
  closeDrawer = () => {
	this.drawer._root.close()
  };
	
  openDrawer = () => {
	this.drawer._root.open()
  };

  render() {
    return (
    	<Drawer
          ref={(ref) => { this.drawer = ref; }}
          content={<SideBar  />}
          onClose={() => this.closeDrawer()} >
        <MainHeader openDrawer={() => this.openDrawer()} title="Edukasi G1R1J" />
        <Container style={{ flex: 1, padding:10, alignItems:"flex-start" }}>
          <Content>
            <NotificationListItem title={"1. G1R1J adalah ..."} content={"Lorem Ipsum"} />
            <NotificationListItem title={"2. G1R1J adalah ..."} content={"Lorem Ipsum"} />
            <NotificationListItem title={"3. G1R1J adalah ..."} content={"Lorem Ipsum"} />
            <NotificationListItem title={"4. G1R1J adalah ..."} content={"Lorem Ipsum"} />
          </Content>
        </Container>
      </Drawer>
    );
  }
}

Drawer.defaultProps.styles.mainOverlay.elevation = 2;