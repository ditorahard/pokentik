import React, {Component} from 'react';
import { View, Image, Share, FlatList } from 'react-native';
import { Container, Content, Text, Body, Drawer, Spinner } from "native-base";
import MainHeader from '../../components/Header/MainHeader.component';
import SideBar from '../../components/Sidebar/Sidebar.component';
import HistoryListItem from '../../components/ListItem/History/HistoryListItem.component';
import styles from './History.container.style';
import axios from 'axios';
import deviceStorage from '../../services/deviceStorage';
import NavigationService from '../../routes/NavigationService';

export default class History extends React.Component {
  constructor(props) {
        super(props);
        this.state = {
          id:'',
          page:1,
          limit:10,
          activity:null,
          refresh:false,
          loadMore:false,
        }
    }
   componentDidMount(){
      if(this.state.id==''){      
      deviceStorage.takeItem('id').then((response) => {
        let newId = response;
        this.setState({
          id: newId 
        });
        //console.warn("id", this.state.id);
        var activityDetail = {};
        // activityDetail.userId = this.state.id;
        activityDetail.userId = this.state.id;
        activityDetail.page = this.state.page;
        activityDetail.limit = this.state.limit;
        this._getActivity(activityDetail);
      });
    }
  }

  _getActivity = (activityDetail) => {
      axios
        .post('https://pokentik.com/api/getUserLarvaeNest', activityDetail)
        .then((response) => {
          //console.warn("Ini activity",response.data.data);
          var newActivity = response.data.data;
          this.setState({
            activity: newActivity,
            refresh:false,
            loadMore:false,
          });
        })
        .catch((error) =>{
          console.warn("Error user", error.response);
        })
    }

    _nextActivity = (newLimit) => {
      this.setState({
        limit: newLimit
      })
      var activityDetail = {};
      // activityDetail.userId = this.state.id;
    activityDetail.userId = this.state.id;
    activityDetail.page = this.state.page;
    activityDetail.limit = this.state.limit;
    this._getActivity(activityDetail);
    }

    _handleRefresh = () => {
      var thisEntity = this;
      thisEntity.setState({
        refresh:true,
        page:1,
        limit:10,
      },
      () => {
        let activityDetail = {};
        activityDetail.userId = thisEntity.state.id;
        activityDetail.page = thisEntity.state.page;
        activityDetail.limit = thisEntity.state.limit;
        thisEntity._getActivity(activityDetail);
      });
    }

    _handleLoadMore = () => {
      var thisEntity = this;
      thisEntity.setState({
        loadMore:true,
        limit:thisEntity.state.limit + 10,
      },
      () => {
        let activityDetail = {};
        activityDetail.userId = thisEntity.state.id;
        activityDetail.page = thisEntity.state.page;
        activityDetail.limit = thisEntity.state.limit;
        thisEntity._getActivity(activityDetail);
      });
    }

    _onShareClick(activity, idActivity) {
      Share.share({
        message: 'Pembersihan jentik nyamuk di '+ activity + ' telah berhasil dilakukan! Anda mendapat pesan ini dari aplikasi Pokentik http://pokentik.com/activityImage/' + idActivity,
        url: 'http://www.pokentik.com',
        title: 'Pokentik'
      }, {
        // Android only:
        dialogTitle: 'Share Aktivitas Pokentik',
        // iOS only:
        excludedActivityTypes: [
          'com.apple.UIKit.activity.PostToTwitter'
        ]
      })
    }

  closeDrawer = () => {
    this.drawer._root.close()
  };
  
  openDrawer = () => {
    this.drawer._root.open()
  };

  render() {
    var thisEntity = this;
    var activityList = null;
    var noActivity = (<Text>Anda belum memiliki aktivitas</Text>);
    var loading = (<Spinner color="#18B5AE" />);

    // if (this.state.activity) {
    //         activityList = this.state.activity.map(function (item, i) {
    //         var activityTitle=item.city + ", " + item.district + ", " + item.village 
    //             return (
    //               <HistoryListItem key={i} 
    //               title={activityTitle} 
    //               subtitle={item.observing_date} 
    //               action={() => { NavigationService.navigate('ActivityDetail', {idActivity:item.id}); }} 
    //               actionShare={()=>{thisEntity.onShareClick(activityTitle)}}/>);
    //     });
    // }

    if (this.state.activity) {
            activityList = (
              <FlatList
                data={this.state.activity}
                renderItem={({ item }) => (
                  <HistoryListItem
                  title={item.city + ", " + item.district + ", " + item.village} 
                  subtitle={item.observing_date} 
                  action={() => { NavigationService.navigate('ActivityDetail', {idActivity:item.id}); }} 
                  actionShare={()=>{thisEntity._onShareClick((item.city + ", " + item.district + ", " + item.village), item.id)}} />
                )}
                keyExtractor={(item, index) => index.toString()}
                refreshing={this.state.refresh}
                onRefresh={this._handleRefresh}
                onEndReached={this._handleLoadMore}
                onEndThreshold={0}

              />
            )
    }

    return (
      <Drawer
          ref={(ref) => { this.drawer = ref; }}
          content={<SideBar  />}
          onClose={() => this.closeDrawer()} >
        <MainHeader openDrawer={() => this.openDrawer()} title="Riwayat" />
        <Container style={{ flex: 1, padding:10, justifyContent:'center' }}>
          <Content>
            {this.state.activity ? 
              (this.state.activity.length > 0 ? 
                activityList 
                : 
                noActivity ) 
              : 
            loading}
            {this.state.loadMore ? loading : null}
          </Content>
        </Container>
      </Drawer>
    );
  }
}

Drawer.defaultProps.styles.mainOverlay.elevation = 2;