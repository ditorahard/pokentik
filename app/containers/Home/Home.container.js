import React, {Component} from 'react';
import { View, Image, Linking, ToastAndroid, FlatList } from 'react-native';
import { Container, Content, Text, Body, Drawer } from "native-base";
import PropTypes from 'prop-types';
import styles from './Home.container.style';
import TransparentButton from '../../components/Buttons/Transparent/TransparentButton.component';
import { StackNavigator } from "react-navigation";
import MainHeader from '../../components/Header/MainHeader.component';
import SideBar from '../../components/Sidebar/Sidebar.component';
import Subheader from '../../components/Subheader/Subheader.component';
import Ad from '../../components/Ad/Ad.component';
import NavigationService from '../../routes/NavigationService';
import deviceStorage from '../../services/deviceStorage';
import axios from 'axios';
// import Sound from 'react-native-sound';

export default class Home extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
        	token:'',
        	account:{},
        	ads:[]
        }
    }

	componentWillMount() {

		
    }

    componentDidMount(){
    	this.getAds();
    	if(this.state.token==''){			
			deviceStorage.takeItem('token').then((response) => {
				let newToken = response;
				this.setState({
					token: newToken 
				});
				//console.warn('toke n home', newToken);
				let getUserInput = {}
				getUserInput.token = newToken;
				this.getUser(getUserInput);
				//console.warn('get user input', getUserInput);
			});	
		}
		else{
			var newToken = this.state.token;
			//console.warn('toke n home established token', newToken);
			let getUserInput = {}
			getUserInput.token = newToken;
			this.getUser(getUserInput);
			//console.warn('get user input', getUserInput);
		}
    }

    getUser = (token) => {
    	axios
	      .post('https://pokentik.com/api/getUser', token)
	      .then((response) => {
	        //console.warn("Ini user",response.data.data);
	        var newAccount = response.data.data;
	        this.setState({
	        	account: newAccount
	        });
	        //console.warn("user id", newAccount.id);
	        deviceStorage.saveItem('id', JSON.stringify(newAccount.id));
	  		// deviceStorage.takeItem('id').then((response) => {
			// console.warn ("ide woy", response);
			// });
	      })
	      .catch((error) =>{
	        console.warn("Error user", error.response);
	      })
    }

    getAds = () => {
    	var page = 1;
    	var limit = 3;
    	var adsInput = {
    		page:page,
    		limit:limit
    	}
    	axios
	      .post('https://pokentik.com/api/getAdsList', adsInput)
	      .then((response) => {
	      	console.warn("Ini ads", response.data.data);
	        var newAds = response.data.data;
	        this.setState({
	        	ads: newAds
	        });
	      })
	      .catch((error) =>{
	        console.warn("Error ads", error.response);
	      })
    }
	
	closeDrawer = () => {
	    this.drawer._root.close()
	};
	
	openDrawer = () => {
	    this.drawer._root.open()
	};

	_renderSeparator = () => {
    return (
      <View
        style = {{
          height: 0,
          width: 200,
          backgroundColor: '#FFFFFF'
        }}
      />
    )
  }         

_renderAdsItem = ({item, index}) => (
  <View style={{flexDirection: 'column'}}>
    <Ad 
    nama={item.name} 
    imageURI={{uri:'https://pokentik.com/public/advertisements/' + item.image}} 
    />
    {(index !== this.state.ads.length - 1) && this._renderSeparator()}
  </View>
)



	_renderAds = () => {
		var thisEntity= this;
		var content = null;
		if(thisEntity.state.ads){
			content = (
				<FlatList
				horizontal={false} 
				data={thisEntity.state.ads}
                renderItem={this._renderAdsItem}
                keyExtractor={(item, index) => index.toString()}
                // refreshing={this.state.refresh}
                // onRefresh={this._handleRefresh}
                // onEndReached={this._handleLoadMore}
                // onEndThreshold={0}
				/>
			);
		}
		return content;
	}

	render() {
		var thisEntity = this;
	    return (
	      <Drawer
          ref={(ref) => { this.drawer = ref; }}
          content={<SideBar closeSidebar={thisEntity.closeDrawer}  />}
          onClose={() => this.closeDrawer()} >
	          <MainHeader openDrawer={() => this.openDrawer()} notification={() => { NavigationService.navigate('Notifications');}} />
			  <Container style={styles.home}>
					<Content contentContainerStyle={{ flexGrow: 1 }}>
						<View style={styles.haloContainer}>	
							<View style={styles.photoContainer}>
								<Image
								  style={styles.profilePhoto}
								  source={this.state.account.user_image!='' ? {uri:this.state.account.image_path} : require('../../assets/img/fotoprofil1.png')}
								/>
							</View>
							<View>
								<Text style={styles.textHalo}>
								  {this.state.account.name ? this.state.account.name : "Loading ... "}
								</Text>
								<Text style={styles.textHalo}>
								  Mari budayakan gerakan
								</Text>
								<Text style={styles.textHalo}>
								  1 rumah 1 jumantik!
								</Text>
							</View>
							<View style={styles.points}>
								 <Text style={styles.pointsText}>
								 	 {this.state.account.point!=null ? this.state.account.point+"" : "00"} pts
								 </Text>
							</View>
						</View>
						<Subheader title="MENU" />
				    	<View style={{flexDirection:'row', justifyContent:'center', alignItems: 'stretch', padding:10}}>				    		
				    		<TransparentButton iconColor="red" action={() => { NavigationService.navigate('Profile');}} style={styles.menuButton} text="Profil" icon="md-contact" />
				    		<TransparentButton 
				    			iconColor="#66BB6A"
				    			action={()=>{NavigationService.navigate('WhatsappGroup', {grupKelurahan:thisEntity.state.account.grupKelurahan, grupKecamatan:thisEntity.state.account.grupKecamatan});}} 
				    			style={styles.menuButton} 
				    			text="Whatsapp" icon="logo-whatsapp" />
				    		<TransparentButton 
				    		iconColor="#29B6F6"
				    		action={()=>{Linking.openURL("https://youtu.be/cC1zal97Si0")}} style={styles.menuButton} text="Mars" icon="ios-musical-notes" />
				    		<TransparentButton 
				    		iconColor="#FFEB3B"
				    		action={() => { NavigationService.navigate('HowToUse');}} style={styles.menuButton} text="Petunjuk" icon="ios-help-circle-outline" />			    		
				    	</View>
				    	<View style={{flexDirection:'row', justifyContent:'center', alignItems: 'stretch', padding:10}}>				    		
				    		<TransparentButton 
				    		iconColor="#FF9800"
				    		action={() => { NavigationService.navigate('ActivityChoices');}} style={styles.menuButton} text="Aktivitas" icon="md-navigate" />
				    		<TransparentButton 
				    		iconColor="#ef5350"
				    		action={() => { NavigationService.navigate('History');}} style={styles.menuButton} text="Riwayat" icon="ios-archive" />
				    		<TransparentButton 
				    		iconColor="#FDD835"
				    		action={() => { Linking.openURL("https://docs.google.com/viewer?url=http%3A%2F%2Fwww.jumantik.org%2Fimages%2Fbook%2FJuknis_1_Rumah_1_Jumantik.pdf");}} style={styles.menuButton} text="Edukasi" icon="ios-bulb" />
				    		<TransparentButton 
				    		iconColor="#3F51B5"
				    		action={() => { Linking.openURL("https://facebook.com/pokentik");}} style={styles.menuButton} text="Fanpage" icon="logo-facebook" />			    		
				    	</View>
				    	<Subheader title="IKLAN LAYANAN MASYARAKAT" />
				    		<View>
				    			{/*<Ad nama="Ad 1" imageURI={require('../../assets/img/mcd.png')} action={() => { Linking.openURL("https://mcdonalds.co.id");}} />
				    								    		<Ad nama="Ad 2" imageURI={require('../../assets/img/esteler.png')} action={() => { Linking.openURL("https://esteler77.com");}} />
				    								    		<Ad nama="Ad 3" imageURI={require('../../assets/img/tokped.png')} action={() => { Linking.openURL("https://tokopedia.com");}} /> */}
				    			{this._renderAds()}
				    		</View>
			    	</Content>
			  </Container>
		  </Drawer>
		)
	}
}

Drawer.defaultProps.styles.mainOverlay.elevation = 2;