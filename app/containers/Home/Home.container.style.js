import {StyleSheet} from 'react-native';
// import theme from '../../styles/theme.style';
// import {headingText, textInput} from '../../styles/common.style';

export default StyleSheet.create({
	home:{
		flex:1
	},
	haloContainer:{
		flexDirection:'row', 
		flexWrap:'wrap',
		paddingTop:20,
		paddingBottom:20,
		paddingLeft:5,
		paddingRight:5,
	},
	textHalo:{
		fontSize:13,
	},
	photoContainer:{
		flex:1,
		height:50,
	},
	profilePhoto:{
		flex: 1,
	    width:50,
	    height:50,
	    borderRadius:50,
	},
	points:{
	    flex: 1,
	    justifyContent: 'center',
	    alignItems: 'center'
	},
	pointsText:{
		fontSize:13,
	},
	adContainer:{
		padding:20,
		flexDirection:'row', 
		flexWrap:'wrap'
	},
	homeContent:{
	},
});