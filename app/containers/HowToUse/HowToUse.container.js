import React, {Component} from 'react';
import { View, Image } from 'react-native';
import { Container, Content, Text, Body, Drawer } from "native-base";
import MainHeader from '../../components/Header/MainHeader.component';
import SideBar from '../../components/Sidebar/Sidebar.component';
import NotificationListItem from '../../components/ListItem/Notifications/NotificationListItem.component';
import styles from './HowToUse.container.style';

export default class HowToUse extends React.Component {
  
  closeDrawer = () => {
	this.drawer._root.close()
  };
	
  openDrawer = () => {
	this.drawer._root.open()
  };

  render() {
    return (
    	<Drawer
          ref={(ref) => { this.drawer = ref; }}
          content={<SideBar  />}
          onClose={() => this.closeDrawer()} >
        <MainHeader openDrawer={() => this.openDrawer()} title="Cara Kerja App" />
        <Container style={{ flex: 1, padding:10, alignItems:"flex-start" }}>
          <Content>
            <NotificationListItem title={"1. Aktivitas"} content={"Tekan tombol aktivitas di halaman home"} />
            <NotificationListItem title={"2. Add Aktivitas"} content={"Pilih Add Aktivitas"} />
            <NotificationListItem title={"3. Pilih titik"} content={"Ketik lokasi atau pilih titik di peta"} />
            <NotificationListItem title={"4. Isi form"} content={"Lengkapi semua isian aktivitas"} />
            <NotificationListItem title={"5. Submit form"} content={"Submit form setelah semua diisi"} />
          </Content>
        </Container>
      </Drawer>
    );
  }
}

Drawer.defaultProps.styles.mainOverlay.elevation = 2;