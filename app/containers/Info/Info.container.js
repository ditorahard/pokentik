import React, {Component} from 'react';
import { View } from 'react-native';
import { Container, Content, Text, Body, Drawer, Button } from "native-base";
import Subheader from '../../components/Subheader/Subheader.component';
import SideBar from '../../components/Sidebar/Sidebar.component';
import Searchbar from '../../components/Searchbar/Searchbar.component';
import styles from './Info.container.style';
import NavigationService from '../../routes/NavigationService';
import PropTypes from 'prop-types';
import MainHeader from '../../components/Header/MainHeader.component';
// import ActivityHeader from '../../components/ActivityHeader/ActivityHeader.component';

export default class Info extends Component {
	render(){
		const { navigation } = this.props;
    	const title = navigation.getParam('title', 'Info');
    	const message = navigation.getParam('message', 'Loading ...');
    	const header = navigation.getParam('header', '');
    	const buttonText = navigation.getParam('buttonText', 'Go to home');
    	
		return(
			<Drawer
          ref={(ref) => { this.drawer = ref; }}
          content={<SideBar  />}
          onClose={() => this.closeDrawer()} >
	      <MainHeader noBack openDrawer={() => this.openDrawer()} title={title ? title : "Info" } />
	      <Container style={{ flex: 1, padding:10, justifyContent: 'center' }}>
		    <Content>
                <Text>
                  {message ? message : "Loading ..."}
                </Text>
                <View>
                  <Button block onPress={()=>{NavigationService.navigate('Home')}} style={styles.registerbutton}>
                    <Text>
                      {buttonText ? buttonText : "Go to home"}
                    </Text>
                  </Button>
                </View>
	        </Content>
	      </Container>
      </Drawer>
		)
	}
}

Info.propTypes = {
  message: PropTypes.string,
  title: PropTypes.string,
  header: PropTypes.string,
  buttonText: PropTypes.string,
}