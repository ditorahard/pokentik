import React, {Component} from 'react';
import { View, Image, ImageBackground, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import RoundedTextForm from '../../components/Forms/RoundedTextForm.component';
import { Text, Button, Content, Spinner } from 'native-base';
import style from './Login.container.style';
import NavigationService from '../../routes/NavigationService';
import axios from 'axios';
import deviceStorage from '../../services/deviceStorage';

export default class Login extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
        	email:'',
        	password:'',
        	loading:false,
        }

        this._submitLogin = this._submitLogin.bind(this);
    }

	componentWillMount() {
        
    }

    componentDidMount(){

    }

    _submitLogin(){
    	var thisEntity = this;
    	thisEntity.setState({
    		loading:true,
    	});
    	var account = {
    		'email': thisEntity.state.email,
    		'password':thisEntity.state.password
    	}
    	console.warn("Akun", account);
    	var url = 'https://pokentik.com/api/login';
    	console.warn("Url", url)
    	axios
	      .post(url, account)
	      .then((response) => {
	        console.warn("Ini login",response.data);
	        var account = response.data.data;

	        console.warn("new token", account.token);
	        deviceStorage.saveItem("token", account.token);
	        deviceStorage.saveItem("id", JSON.stringify(account.id));	        
	      })
	      .then((response) =>{
	      	var testoken = deviceStorage.takeItem("token");
	      	thisEntity.setState({
	    		loading:false,
	    	});
	        console.warn("tes token", testoken);
	        NavigationService.navigate('SignIn');
	      })
	      .catch((error) =>{
	      	thisEntity.setState({
	    		loading:false,
	    	});
	        console.warn("Error login", error.response);
	      })
    	// console.warn("submit Login");
    }

  render() {
  	
  	var thisEntity = this;

    return (
    	<ImageBackground
    	  style={style.imageBackground}
    	  source={require('../../assets/img/gambarlogin2.png')}
    	>
	     <View style={style.login}>
		    {this.state.loading ? 
		    	(<Content>
		    		<Spinner color="#FFFFFF" />
		    	</Content>)
		     :  
		     (<Content>
		        <View style={style.title}>
		        	<Image style={style.logoTop}
		        		source={require('../../assets/img/unnamed.png')}
		        		resizeMode="contain"
		        	/>
		        	<Image style={style.logo}
		        		source={require('../../assets/img/pokentikwhite.png')}
		        		resizeMode="contain"
		        	/>
		        </View> 
		        <View style={style.form}>
			        <RoundedTextForm 
			        	placeholder='Email atau username' 
			        	onChangeText={(email) => this.setState({email})}
			        	value={thisEntity.state.email}
			        	style={style.input}
			        	placeholderColor="white"
			        	textColor="white" 
			        />
		        </View>
		        <View style={style.form}>
			        <RoundedTextForm 
			        	placeholder='Password' 
			        	onChangeText={(password) => this.setState({password})} 
			        	value={thisEntity.state.password}
			        	style={style.input}
			        	placeholderColor="white"
			        	textColor="white"
			        	secure={true}
			        />
		        </View>
		        <View style={{alignSelf:'center', justifyContent:'center'}}>
			        <TouchableOpacity onPress={() => { NavigationService.navigate('ForgotPassword') }}>
			        	<Text style={style.whiteText}>
			        		Lupa Password ?
			        	</Text>
			        </TouchableOpacity>
		        </View>
		        <Button rounded block onPress={thisEntity._submitLogin} style={style.loginbutton}>
		        	<Text>
		        		Login
		        	</Text>
		        </Button>
		        <Button rounded block onPress={() => { NavigationService.navigate('Registration')}} style={style.registerbutton}>
		        	<Text>
		        		Register
		        	</Text>
		        </Button>
		        <View style={{marginTop:10, alignSelf:'center', justifyContent:'center'}}>
		        	<Text style={{color:'#FFFFFF', fontSize:10}}>
		        	  Supported By:
		        	</Text>
		        </View>
		        <View style={{flexDirection:'row', alignSelf:'center', justifyContent:'center'}}>
		        	<Image
		        	  style={style.logoSponsor}
		        	  source={require('../../assets/img/kemenkes.png')}
		        	/>
		        	<Image
		        	  style={style.logoSponsor}
		        	  source={require('../../assets/img/ausalum.png')}
		        	/>
		        </View>
		    </Content>) }
	    </View> 
	    </ImageBackground>
	    );
	}
}

// Login.propTypes = {
//   submitLogin: PropTypes.func.isRequired,
// }
