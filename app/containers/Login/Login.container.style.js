import {StyleSheet} from 'react-native';

export default StyleSheet.create({
	login:{
		flex:1, 
		paddingTop:40,
		paddingLeft:30,
		paddingRight:30,
		// backgroundColor:'#18B5AE',
	},
	title:{
		flex:1,
		width:null,
		height:80,
		textAlign:'center',
		marginBottom:30,
		alignItems: 'center', 
		justifyContent: 'center',
	},
	form:{
		marginBottom:20,
	},
	input:{
		borderRadius:20,
	},
	loginbutton:{
		marginTop:20,
		backgroundColor:'#F7941D',
	},
	logo:{
		flex:1
	},
	logoTop:{
		flex:3,
		marginBottom:10
	},
	registerbutton:{
		marginTop:20,
		backgroundColor:'#008066',
	},
	imageBackground:{
		width:'100%',
		height:'100%',
		alignSelf: "stretch",
        justifyContent: "center",
        alignItems: "stretch",
        resizeMode:"stretch",
	},
	logoSponsor:{
		width:90,
		height:90,
		marginLeft:10,
		resizeMode:"contain",
	},
	whiteText:{
		color:"#FFFFFF",
		textDecorationLine: "underline"
	},
});