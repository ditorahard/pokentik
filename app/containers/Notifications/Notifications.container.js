import React, {Component} from 'react';
import { View, Image } from 'react-native';
import { Container, Content, Text, Body, Drawer, List } from "native-base";
import MainHeader from '../../components/Header/MainHeader.component';
import SideBar from '../../components/Sidebar/Sidebar.component';
import NotificationListItem from '../../components/ListItem/Notifications/NotificationListItem.component';
import styles from './Notifications.container.style';
import deviceStorage from '../../services/deviceStorage';
import axios from 'axios';

export default class Notifications extends React.Component {
  constructor(props) {
        super(props);
        this.state = {
          token:'',
          notifications:[],
        }
  }

  componentDidMount(){
      if(this.state.token==''){;      
        deviceStorage.takeItem('token').then((response) => {
          let newToken = response;
          this.setState({
            token: newToken 
          });
          console.warn('toke n home', newToken);
          this.getUserNotification(newToken);
        }); 
      }
      else{
        var newToken = this.state.token;
        console.warn('toke n home established token', newToken);
        // let getUserInput = {}
        // getUserInput.token = newToken;
        this.getUserNotification(newToken);
      }
    }

    getUserNotification = (token) => {
      axios
        .get('https://pokentik.com/api/getUserNotification/'+ token)
        .then((response) => {
          console.warn("Ini notifikasi",response.data.data);
          var notifications = response.data.data;
          this.setState({
            notifications: notifications
          });
        })
        .catch((error) =>{
          console.warn("Error notification", error.response);
        })
    }

  closeDrawer = () => {
	this.drawer._root.close()
  };
	
  openDrawer = () => {
	this.drawer._root.open()
  };

  render() {
    var notificationslist = null;
    if (this.state.notifications.length>0) {
            notificationslist = this.state.notifications.map(function (item, i) { 
                return (<NotificationListItem key={i} title={item.title} content={item.content} />);
        });
    }
    return (
    	<Drawer
          ref={(ref) => { this.drawer = ref; }}
          content={<SideBar  />}
          onClose={() => this.closeDrawer()} >
        <MainHeader openDrawer={() => this.openDrawer()} title="Notifikasi" />
        <Container style={{ flex: 1, padding:10, alignItems:"flex-start" }}>
          <Content>
          <List>
            {notificationslist}
          </List>
          </Content>
        </Container>
      </Drawer>
    );
  }
}

Drawer.defaultProps.styles.mainOverlay.elevation = 2;