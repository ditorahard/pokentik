import {StyleSheet} from 'react-native';
// import theme from '../../styles/theme.style';
// import {headingText, textInput} from '../../styles/common.style';

export default StyleSheet.create({
	home:{
		flex: 1, 
		//alignItems: 'center', 
		justifyContent: 'center',
		paddingTop:60,
		marginBottom:10,
	},
	hero:{
		paddingTop:50,
		paddingBottom:50,
		alignItems:'center',
		backgroundColor:'#014DB2',
	},
	whitetext:{
		color:'#FFFFFF',
		fontSize:20,
		fontWeight:'700',
	},
	ajardiaLogo:{
		 flex: 1,
	     resizeMode: 'contain'
	},
	logoContainer:{
		height:100,
	},
});