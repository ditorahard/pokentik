import React, {Component} from 'react';
import { View } from 'react-native';
import { Header, Container, Content, Text, Body, Drawer, Tab, Tabs, Button } from "native-base";
import PropTypes from 'prop-types';
import style from './Profile.container.style';
import { StackNavigator } from "react-navigation";
import MainHeader from '../../components/Header/MainHeader.component';
import SideBar from '../../components/Sidebar/Sidebar.component';
import ProfileDetail from '../../components/Profile/Detail/ProfileDetail.component';
import NavigationService from '../../routes/NavigationService';
import deviceStorage from '../../services/deviceStorage';
import axios from 'axios';

export default class ProfileOverview extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
        	token:'',
        	account:{},
        }
    }

	componentDidMount(){
    	if(this.state.token==''){;			
			deviceStorage.takeItem('token').then((response) => {
				let newToken = response;
				this.setState({
					token: newToken 
				});
				console.warn('toke n home', newToken);
				let getUserInput = {}
				getUserInput.token = newToken;
				this.getUser(getUserInput);
				console.warn('get user input', getUserInput);
			});	
		}
		else{
			var newToken = this.state.token;
			console.warn('toke n home established token', newToken);
			let getUserInput = {}
			getUserInput.token = newToken;
			this.getUser(getUserInput);
			console.warn('get user input', getUserInput);
		}
    }

    getUser = (token) => {
    	axios
	      .post('https://pokentik.com/api/getUser', token)
	      .then((response) => {
	        console.warn("Ini user",response.data.data);
	        var newAccount = response.data.data;
	        this.setState({
	        	account: newAccount
	        });
	      })
	      .catch((error) =>{
	        console.warn("Error user", error.response);
	      })
    }
	

	closeDrawer = () => {
	    this.drawer._root.close()
	};
	
	openDrawer = () => {
	    this.drawer._root.open()
	};

	render() {
		var thisEntity = this;
		var sex = null;
		if(thisEntity.state.account.sex){
			sex = JSON.parse(JSON.stringify(thisEntity.state.account.sex))
		}
	    return (
	      <Drawer
          ref={(ref) => { this.drawer = ref; }}
          content={<SideBar closeSidebar={thisEntity.closeDrawer} />}
          onClose={() => this.closeDrawer()} >
	          <MainHeader openDrawer={() => this.openDrawer()} notification={() => { NavigationService.navigate('Notifications');}} />
			  <ProfileDetail 
			  name={this.state.account.name} 
			  email={this.state.account.email} 
			  points={this.state.account.point}
			  profileImage={this.state.account.user_image}
			  />
			  <Header hasTabs style={style.tabHeader} androidStatusBarColor="#18B5AE"/>
			  <Tabs>
	          <Tab heading="PROFIL" tabStyle={style.tabTitle} textStyle={style.tabText} activeTabStyle={style.activeTabTitle} activeTextStyle={style.activeTabText}>
	            <Container>
					<Content contentContainerStyle={style.profileInfo}>
						<View style={style.profileInfoSegment}>
							<Text style={style.infoHeader}>Email</Text>
							<Text style={style.infoContent}>{this.state.account.email ? this.state.account.email  : "Loading ..."}</Text>
						</View>
						<View style={style.profileInfoSegment}>
							<Text style={style.infoHeader}>Tanggal Lahir</Text>
							<Text style={style.infoContent}>{this.state.account.birth_date ? this.state.account.birth_date  : "Loading ..."}</Text>
						</View>
						<View style={style.profileInfoSegment}>
							<Text style={style.infoHeader}>Jenis Kelamin</Text>
							<Text style={style.infoContent}>{sex ? "Pria"  : "Wanita"}</Text>
						</View>
						<View style={style.profileInfoSegment}>
							<Text style={style.infoHeader}>Nomor Handphone</Text>
							<Text style={style.infoContent}>{this.state.account.phone_number ? this.state.account.phone_number  : "Loading ..."}</Text>
						</View>
					</Content>
			  </Container>
	          </Tab>
	          <Tab heading="POIN" tabStyle={style.tabTitle} textStyle={style.tabText} activeTabStyle={style.activeTabTitle} activeTextStyle={style.activeTabText}>
	            <Container>
					<Content contentContainerStyle={style.profileInfo}>
						{/*<View style={style.profileInfoSegment}>
							<Text style={style.infoHeader}>Jumlah Aktifitas</Text>
							<Text style={style.infoContent}>123</Text>
						</View>*/}
						<View style={style.profileInfoSegment}>
							<Text style={style.infoHeader}>Total Poin Aktifitas</Text>
							<Text style={style.infoContent}>{this.state.account.point!=null ? this.state.account.point+" pts"  : "Loading ..."}</Text>
						</View>
						<View style={style.profileInfoSegment}>
							<Text style={style.infoHeader}>Redeem</Text>
							<Button style={style.redeemButton} onPress={() => {NavigationService.navigate('Redeem')}}><Text>Redeem Points</Text></Button>
						</View>
					</Content>
			  </Container>
	          </Tab>
	        </Tabs>
		  </Drawer>
		)
	}
}

Drawer.defaultProps.styles.mainOverlay.elevation = 0;

// Profile.propTypes = {
// 	coba: PropTypes.string,
// 	toCourses: PropTypes.func,
// 	toLeaderboard: PropTypes.func,
// 	toProfile: PropTypes.func,
// 	toHome: PropTypes.func,
// };