import React, {Component} from 'react';
import { View, Image, Share, FlatList, TouchableOpacity, ToastAndroid } from 'react-native';
import { Container, Content, Text, Body, Drawer, Spinner } from "native-base";
import MainHeader from '../../components/Header/MainHeader.component';
import SideBar from '../../components/Sidebar/Sidebar.component';
import styles from './Redeem.container.style';
import axios from 'axios';
import deviceStorage from '../../services/deviceStorage';
import NavigationService from '../../routes/NavigationService';
import ImageResizeMode from 'react-native/Libraries/Image/ImageResizeMode';
import Subheader from '../../components/Subheader/Subheader.component';
import ActivityHeader from '../../components/ActivityHeader/ActivityHeader.component';
import RedeemItem from '../../components/RedeemItem/RedeemItem.component';

export default class Redeem extends React.Component {
  constructor(props) {
        super(props);
        this.state = {
        token:'',
          id:'',
          page:1,
          limit:10,
          redeemList:null,
          refresh:false,
          loadMore:false,
        }
    }
   componentDidMount(){
      if(this.state.id==''){      
      deviceStorage.takeItem('id').then((response) => {
        let newId = response;
        this.setState({
          id: newId 
        });
        //console.warn("id", this.state.id);
        var redeemDetail = {};
        // redeemDetail.userId = this.state.id;
        redeemDetail.page = this.state.page;
        redeemDetail.limit = this.state.limit;
        this._getRedeemList(redeemDetail);
      });
    }
    if(this.state.token==''){;			
			deviceStorage.takeItem('token').then((response) => {
				let newToken = response;
				this.setState({
					token: newToken 
				});
				console.warn('toke n home', newToken);
				let getUserInput = {}
				getUserInput.token = newToken;
				this._getUser(getUserInput);
				console.warn('get user input', getUserInput);
			});	
		}
		else{
			var newToken = this.state.token;
			console.warn('toke n home established token', newToken);
			let getUserInput = {}
			getUserInput.token = newToken;
			this._getUser(getUserInput);
			console.warn('get user input', getUserInput);
	}
  }

	_getUser = (token) => {
    	axios
	      .post('https://pokentik.com/api/getUser', token)
	      .then((response) => {
	        console.warn("Ini user",response.data.data);
	        var newAccount = response.data.data;
	        this.setState({
	        	account: newAccount
	        });
	      })
	      .catch((error) =>{
	        console.warn("Error user", error.response);
	      })
   }

  _getRedeemList = (redeemDetail) => {
      axios
        .post('https://pokentik.com/api/redeemItemList', redeemDetail)
        .then((response) => {
          console.warn("Ini redeem list",response.data.data);
          var newRedeemList = response.data.data;
          this.setState({
            redeemList: newRedeemList,
            refresh:false,
            loadMore:false,
          });
        })
        .catch((error) =>{
          console.warn("Error get redeem list", error.response);
        })
    }

    _nextRedeemList = (newLimit) => {
      this.setState({
        limit: newLimit
      })
      var redeemDetail = {};
    redeemDetail.page = this.state.page;
    redeemDetail.limit = this.state.limit;
    this._getRedeemList(redeemDetail);
    }

    _handleRefresh = () => {
      var thisEntity = this;
      thisEntity.setState({
        refresh:true,
        page:1,
        limit:10,
      },
      () => {
        let redeemDetail = {};
        redeemDetail.page = thisEntity.state.page;
        redeemDetail.limit = thisEntity.state.limit;
        thisEntity._getRedeemList(redeemDetail);
      });
    }

    _handleLoadMore = () => {
      var thisEntity = this;
      thisEntity.setState({
        loadMore:true,
        limit:thisEntity.state.limit + 10,
      },
      () => {
        let redeemDetail = {};
        redeemDetail.page = thisEntity.state.page;
        redeemDetail.limit = thisEntity.state.limit;
        thisEntity._getRedeemList(redeemDetail);
      });
    }

    _onRedeemClick(price, itemId) {
    	var thisEntity = this;
	    var redeemItem = {
	      	price: price,
	      	item_id: itemId,
	      	quantity: 1,
	      	user_id: thisEntity.state.user_id
	    }
	    axios
	        .post('https://pokentik.com/api/redeemItem', redeemItem)
	        .then((response) => {
	          console.warn("Ini beli",response.data.data);
	          // var newRedeemList = response.data.data;
	          // this.setState({
	          //   redeemList: newRedeemList,
	          //   refresh:false,
	          //   loadMore:false,
	          // });
	          if(response && response.status!=404){
		        NavigationService.navigate('Info', {message:"Redeem Item Berhasil",header:" ",title:"Success",buttonText:"LANJUT KE HOME"});  
		      }
		      else{
		        ToastAndroid.show("Redeem item gagal, sepertinya poin anda tidak cukup.", ToastAndroid.SHORT);
		      } 
	        })
	        .catch((error) =>{
	          console.warn("Error on redeem click", error.response);
	          ToastAndroid.show("Redeem item gagal, sepertinya poin anda tidak cukup.", ToastAndroid.SHORT);
	        })
    }

  closeDrawer = () => {
    this.drawer._root.close()
  };
  
  openDrawer = () => {
    this.drawer._root.open()
  };

  render() {
    var thisEntity = this;
    var redeemListItems = null;
    var noItems = (<Text>Tidak ada list barang</Text>);
    var loading = (<Spinner color="#18B5AE" />);
    var backup = (<TouchableOpacity
                  onPress={()=>{thisEntity._onRedeemClick(item.price, item.id)}}>
                  	<View style={styles.image}> 
			           <Image
			                source={{uri:'pokentik.com/public/products/Kaos_Polo_Baju_Polo_Polo_Shirt_Kaos_Polos_Kaos_Distro_Polos_.png'}}
			                style={{
			                  height: 100,
			                  width:100
			                }}
			                 resizeMode={ImageResizeMode.contain}
			                >
			          </Image>
			        </View>
                  </TouchableOpacity>);

    if (this.state.redeemList) {
            redeemListItems = (
              <FlatList
                data={this.state.redeemList}
                renderItem={({ item }) => (
	                <RedeemItem 
	                  title={item.name} 
	                  subtitle={item.price} 
	                  actionShare={()=>{thisEntity._onRedeemClick(item.price, item.id)}}
	                  action={()=>{thisEntity._onRedeemClick(item.price, item.id)}} 
	                  image={item.image}
	                />
                )}
                keyExtractor={(item, index) => index.toString()}
                refreshing={this.state.refresh}
                onRefresh={this._handleRefresh}
                onEndReached={this._handleLoadMore}
                onEndThreshold={0}
              />
            )
    }

    return (
      <Drawer
          ref={(ref) => { this.drawer = ref; }}
          content={<SideBar  />}
          onClose={() => this.closeDrawer()} >
        <MainHeader openDrawer={() => this.openDrawer()} title="Redeem" />
        <ActivityHeader 
        title={this.state.account ? this.state.account.name : "Loading..."}
        subtitle={this.state.account ? this.state.account.point + ' pts' : "Loading..."} />
        <Container style={{ flex: 1, padding:10, justifyContent:'center' }}>
          <Content>
            {this.state.redeemList ? 
              (this.state.redeemList.length > 0 ? 
                redeemListItems 
                : 
                noItems ) 
              : 
            loading}
            {this.state.loadMore ? loading : null}
          </Content>
        </Container>
      </Drawer>
    );
  }
}

Drawer.defaultProps.styles.mainOverlay.elevation = 2;