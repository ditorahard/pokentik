import React, {Component} from 'react';
import { KeyboardAvoidingView, View, Image, ToastAndroid } from 'react-native';
import { Container, Content, Text, Body, Drawer, Button, ListItem, Left, Right, Spinner } from "native-base";
import Subheader from '../../components/Subheader/Subheader.component';
import SideBar from '../../components/Sidebar/Sidebar.component';
import Searchbar from '../../components/Searchbar/Searchbar.component';
import UnderlinePicker from '../../components/Forms/Underline/UnderlinePicker.component';
import UnderlineTextForm from '../../components/Forms/Underline/UnderlineTextForm.component';
import styles from './Registration.container.style';
import axios from 'axios';
import moment from 'moment';
import NavigationService from '../../routes/NavigationService';
import DateTimePicker from 'react-native-modal-datetime-picker';
import ImagePicker from 'react-native-image-picker';

export default class Registration extends React.Component {
  constructor(props) {
        super(props);
        this.state = {
          name:'',
          email:'',
          password:'',
          birth_place:'',
          birth_date:'',
          sex:'1',
          sexOptions:[
            {name:'Pria', id:'1'},
            {name:'Wanita', id:'0'}
          ],
          phone_number:'',
          kelurahan:'',
          kelurahanOptions:[
            {name:'Pilih kecamatan terlebih dahulu', id:'99999'},
          ],
          kecamatan:'',
          kecamatanOptions:[
            {name:'Pilih kabupaten terlebih dahulu', id:'99999'},
          ],
          kab_kota:'',
          kab_kotaOptions:[
            {name:'Pilih provinsi terlebih dahulu', id:'99999'},
          ],
          provinsi:'',
          provinsiOptions:[],
          alamat:'',
          isRegistered:false,
          isDateTimePickerVisible:false,
          user_image:null,
          imageLoadingSebelum:false,
        }
  }

  componentDidMount(){
    var thisEntity = this;
    var newDate = moment().format('YYYY-MM-DD');
    this._getProvinsiFirst();
    this._getKab_KotaFirst(11);
    this._getKecamatanFirst(1);
    this._getKelurahanFirst(25);
    this.setState({
      birth_date:newDate
    });
  }

  closeDrawer = () => {
	 this.drawer._root.close()
  };
	
  openDrawer = () => {
	 this.drawer._root.open()
  };

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = (date) => {
    let birthDate = moment(date).format('YYYY-MM-DD')
    this.setState({
      birth_date:birthDate
    })
    this._hideDateTimePicker();
  };

  _getProvinsiFirst = () => {
    axios
        .get('https://pokentik.com/api/getProvinsi')
        .then((response) => {
          var newProvinsiOptions = response.data;
          this.setState({
            provinsiOptions: newProvinsiOptions,
            provinsi: newProvinsiOptions[0].id
          });
          return response.data;
        })
        .catch((error) =>{
          console.warn("Error notification", error.response);
        })
  }

  _getProvinsi = () => {
    axios
        .get('https://pokentik.com/api/getProvinsi')
        .then((response) => {
          var newProvinsiOptions = response.data;
          this.setState({
            provinsiOptions: newProvinsiOptions,
          });
          return response.data;
        })
        .catch((error) =>{
          console.warn("Error notification", error.response);
        })
  }

  _getKab_Kota = (provinsi) => {
    axios
        .get('https://pokentik.com/api/getKabupatenKota/'+ provinsi)
        .then((response) => {
          var newKabKotaOptions = response.data;
          this.setState({
            kab_kotaOptions: newKabKotaOptions
          });
        })
        .catch((error) =>{
          console.warn("Error notification", error.response);
        })
  }

  _getKab_KotaFirst = (provinsi) => {
    axios
        .get('https://pokentik.com/api/getKabupatenKota/'+ provinsi)
        .then((response) => {
          console.warn("tes", response.data[0])
          var newKabKotaOptions = response.data;
          this.setState({
            kab_kotaOptions: newKabKotaOptions,
            kab_kota: newKabKotaOptions[0]
          });
        })
        .catch((error) =>{
          console.warn("Error notification", error.response);
        })
  }


  _getKecamatan = (kab_kota) => {
    axios
        .get('https://pokentik.com/api/getKecamatan/'+ kab_kota)
        .then((response) => {
          var newKecamatanOptions = response.data;
          this.setState({
            kecamatanOptions: newKecamatanOptions
          });
        })
        .catch((error) =>{
          console.warn("Error notification", error.response);
        })
  }

  _getKecamatanFirst = (kab_kota) => {
    axios
        .get('https://pokentik.com/api/getKecamatan/'+ kab_kota)
        .then((response) => {
          var newKecamatanOptions = response.data;
          this.setState({
            kecamatanOptions: newKecamatanOptions,
            kecamatan:newKecamatanOptions[0]
          });
        })
        .catch((error) =>{
          console.warn("Error notification", error.response);
        })
  }


  _getKelurahan = (kecamatan) => {
    axios
        .get('https://pokentik.com/api/getKelurahan/'+ kecamatan)
        .then((response) => {
          // console.warn("Ini notifikasi",response.data);
          var newKelurahanOptions = response.data;
          this.setState({
            kelurahanOptions: newKelurahanOptions
          });
        })
        .catch((error) =>{
          console.warn("Error notification", error.response);
        })
  }

  _getKelurahanFirst = (kecamatan) => {
    axios
        .get('https://pokentik.com/api/getKelurahan/'+ kecamatan)
        .then((response) => {
          // console.warn("Ini notifikasi",response.data);
          var newKelurahanOptions = response.data;
          this.setState({
            kelurahanOptions: newKelurahanOptions,
            kelurahan:newKelurahanOptions[1],
          });
        })
        .catch((error) =>{
          console.warn("Error notification", error.response);
        })
  }

  _pickImageHandler = (imageStateName, loadingStateName) => {
    console.warn("nama state image", imageStateName);
    this.setState({
            [loadingStateName]: true,
    });
    const options = {
      title: 'Upload gambar',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.warn('Response = ', response);
      
      if (response.didCancel) {
        console.warn('User cancelled image picker');
        this.setState({
            [loadingStateName]: false,
        });
      } else if (response.error) {
          console.warn('ImagePicker Error: ', response.error);
          this.setState({
            [loadingStateName]: false,
          });
      } else if (response.customButton) {
          console.warn('User tapped custom button: ', response.customButton);
          this.setState({
            [loadingStateName]: false,
          });
      } else {
          // const source = { uri: response.uri };
          const source = response;
          console.warn('Source = ', source);
          this.setState({
            [imageStateName]: source,
            [loadingStateName]: false,
          });
      }
    });
  }

  _submitRegister = () => {
      // var newAccount = JSON.parse(JSON.stringify(thisEntity.state));
      // Duh harusnya gak gini ini jelek bgt kodingannya
      
      var thisEntity = this;
      let newAccount = new FormData();

      newAccount.append("name", thisEntity.state.name);
      newAccount.append("email", thisEntity.state.email);
      newAccount.append("password", thisEntity.state.password);
      newAccount.append("birth_place", thisEntity.state.birth_place);
      newAccount.append("birth_date", thisEntity.state.birth_date);
      newAccount.append("sex", thisEntity.state.sex);
      newAccount.append("phone_number", thisEntity.state.phone_number);
      newAccount.append("kelurahan", thisEntity.state.kelurahan);
      newAccount.append("kecamatan", thisEntity.state.kecamatan);
      newAccount.append("kab_kota", thisEntity.state.kab_kota);
      newAccount.append("provinsi", thisEntity.state.provinsi);
      newAccount.append("alamat", thisEntity.state.alamat);
      newAccount.append("user_image", {
        uri:thisEntity.state.user_image.uri,
        type: 'image/jpeg', // or photo.type
        name: 'profpic' + thisEntity.state.user_image.fileName
      });

      // var newAccount ={}
      // newAccount.name = thisEntity.state.name;
      // newAccount.email = thisEntity.state.email;
      // newAccount.password = thisEntity.state.password;
      // newAccount.birth_place = thisEntity.state.birth_place;
      // newAccount.birth_date = thisEntity.state.birth_date;
      // newAccount.sex = thisEntity.state.sex;
      // newAccount.phone_number = thisEntity.state.phone_number;
      // newAccount.kelurahan = thisEntity.state.kelurahan;
      // newAccount.kecamatan = thisEntity.state.kecamatan;
      // newAccount.kab_kota = thisEntity.state.kab_kota;
      // newAccount.provinsi = thisEntity.state.provinsi;
      // newAccount.alamat = thisEntity.state.alamat;

      console.warn("Akun Register", newAccount);
      // axios
      //   .post('https://pokentik.com/api/register', newAccount)
      //   .then((response) => {
      //     // console.warn("Ini register",response.data);
      //     this.setState({isRegistered:true});    
      //   })
      //   .catch((error) =>{
      //     console.warn("Error register", error.response);
      //     ToastAndroid.show("Registrasi error, pastikan email anda benar, atau email anda mungkin sudah terdaftar sebelumnya.", ToastAndroid.SHORT);
      //   })

      fetch('https://pokentik.com/api/register', {  
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data'
      },
      method: 'POST',
      body: newAccount
    })
    .then(response => {
      console.warn("Ini register",response);
      this.setState({isRegistered:true}); 
    })
    .catch(error => {
      console.warn("Error register", error);
      ToastAndroid.show("Registrasi error, pastikan email anda benar, atau email anda mungkin sudah terdaftar sebelumnya.", ToastAndroid.SHORT);
    });
  }

  render() {
    
    let thisEntity=this;
    let todayDate = moment().format('YYYY-MM-DD');
    var loading = (<Spinner color="#18B5AE" />);

    return (
    	<Drawer
          ref={(ref) => { this.drawer = ref; }}
          content={<SideBar  />}
          onClose={() => this.closeDrawer()} >
          <KeyboardAvoidingView style={styles.hero}>
          	<KeyboardAvoidingView style={styles.logoContainer}>
          		<Image
          		style={styles.ajardiaLogo} 
	        		source={require('../../assets/img/pokentikwhite.png')}
	        	/>
              </KeyboardAvoidingView>
          </KeyboardAvoidingView>
	      <Container style={{ flex: 1, padding:10, justifyContent: 'center' }}>
		    <Content>
        {!this.state.isRegistered ? 
		        (<View>
            <Text style={styles.textQuestion}>Nama</Text>
            <UnderlineTextForm placeholder="Nama" onChangeText={(name) => this.setState({name})} value={thisEntity.state.name} />
            {this.state.name==''?
              <Text style={{color:'red',  marginTop:10, marginBottom:10, fontSize:10,}}>
                Nama wajib diisi  
              </Text>: null}
            <Text style={styles.textQuestion}>Email</Text>
		        <UnderlineTextForm placeholder="Email" keyboardType="email-address" onChangeText={(email) => this.setState({email})} value={thisEntity.state.email} />
            {this.state.email==''?
              <Text style={{color:'red', marginTop:10, marginBottom:10, fontSize:10}}>
                Email wajib diisi. Tidak bisa mendaftar email yang sama dua kali
              </Text> : null}
              <Text style={{marginTop:10, marginBottom:10, fontSize:10}}>
                Pastikan email diisi dengan benar (contoh : abc@abc.com) dan belum pernah terdaftar sebelumnya
              </Text>
            <Text style={styles.textQuestion}>Password</Text>
		        <UnderlineTextForm placeholder="Password" secure={true} onChangeText={(password) => this.setState({password})} value={thisEntity.state.password} />
            {this.state.password.length<6?
              <Text style={{color:'red', marginTop:10, marginBottom:10, fontSize:10}}>
                Password minimal 6 karakter
              </Text> : null}
            <Text style={styles.textQuestion}>Tempat Lahir</Text>
            <UnderlineTextForm placeholder="Tempat Lahir" onChangeText={(birth_place) => this.setState({birth_place})} value={thisEntity.state.birth_place} />
            {this.state.birth_place==''?
              <Text style={{color:'red', marginTop:10, marginBottom:10, fontSize:10}}>
                Tempat Lahir wajib diisi
              </Text> : null}
            <ListItem noIndent style={{marginTop:20, marginBottom:20, borderBottom:1}}>
              <Left>
                  <Text>Tanggal Lahir</Text>
              </Left>
              <Body>
                <Button bordered onPress={this._showDateTimePicker}>
                  <Text>{this.state.birth_date}</Text>
                </Button>
              </Body>
              <DateTimePicker
                isVisible={this.state.isDateTimePickerVisible}
                onConfirm={this._handleDatePicked}
                onCancel={this._hideDateTimePicker}
                datePickerModeAndroid="spinner"
              />
            </ListItem>
            {this.state.birth_date==todayDate?
              <Text style={{color:'red', marginTop:10, marginBottom:10, fontSize:10}}>
                Tanggal lahir wajib diisi, tidak bisa hari ini
              </Text> : null}
            {/*<UnderlineTextForm placeholder="Tanggal Lahir" onChangeText={(birth_date) => this.setState({birth_date})} value={thisEntity.state.birth_date} />*/}
            <Text style={styles.textQuestion}>Jenis Kelamin</Text>
            <UnderlinePicker placeholder="Jenis Kelamin" onValueChange={(sex) => this.setState({sex})} selected={thisEntity.state.sex} options={thisEntity.state.sexOptions} />
            <Text style={styles.textQuestion}>Nomor Telepon</Text>
            <UnderlineTextForm placeholder="Nomor Telepon" keyboardType="phone-pad" onChangeText={(phone_number) => this.setState({phone_number})} value={thisEntity.state.phone_number} />
            {this.state.phone_number==''?
              <Text style={{color:'red', marginTop:10, marginBottom:10, fontSize:10}}>
                Nomor telepon wajib diisi
              </Text> : null}
            <Text style={styles.textQuestion}>Provinsi</Text>
            <UnderlinePicker placeholder="Provinsi" onValueChange={(provinsi) => {this.setState({provinsi}, () => thisEntity._getKab_Kota(provinsi));}} selected={thisEntity.state.provinsi} options={thisEntity.state.provinsiOptions} />
            <Text style={styles.textQuestion}>Kabupaten/Kota</Text>
            <UnderlinePicker placeholder="Kabupaten/Kota" onValueChange={(kab_kota) => {this.setState({kab_kota}, () => thisEntity._getKecamatan(kab_kota));}} selected={thisEntity.state.kab_kota} options={thisEntity.state.kab_kotaOptions} />
            <Text style={styles.textQuestion}>Kecamatan</Text>
            <UnderlinePicker placeholder="Kecamatan" onValueChange={(kecamatan) => {this.setState({kecamatan}, () => thisEntity._getKelurahan(kecamatan));}} selected={thisEntity.state.kecamatan} options={thisEntity.state.kecamatanOptions} />            
            <Text style={styles.textQuestion}>Kelurahan</Text>
            <UnderlinePicker placeholder="Kelurahan" onValueChange={(kelurahan) => {this.setState({kelurahan});}} selected={thisEntity.state.kelurahan} options={thisEntity.state.kelurahanOptions} />           
            <Text style={styles.textQuestion}>Alamat</Text>
            <UnderlineTextForm placeholder="Alamat" onChangeText={(alamat) => this.setState({alamat})} value={thisEntity.state.alamat} />
            {this.state.alamat==''?
              <Text style={{color:'red', marginTop:10, marginBottom:10, fontSize:10}}>
                Alamat wajib diisi
              </Text> : null}

            {/*Tombol image sebelum*/}
          <Button success style={styles.formButtonUpload} onPress={()=>{this._pickImageHandler("user_image","imageLoadingSebelum")}} >
            <Text>
              Upload foto profil
            </Text>
          </Button>
          {//Nama gambar, dan loading
            this.state.imageLoadingSebelum ? 
              loading
              : 
              <UnderlineTextForm disabled={true} 
              placeholder="Upload foto profil . . ." 
              value={this.state.user_image&&this.state.user_image.uri ? this.state.user_image.uri : null} /> 
          }
          {this.state.user_image==null&&this.state.imageLoadingSebelum==false ?
              <Text style={{color:'red',  marginTop:10, marginBottom:10, fontSize:10,}}>
                Foto profil wajib di upload  
              </Text>
              : 
            null}

            {this.state.user_image&&this.state.user_image.uri?
              <Image
              style={{marginTop:20, width:100,height:100,resizeMode:'contain'}}
              source={{uri: this.state.user_image.uri}}
            />
            :
            null
            }

            <Button block onPress={thisEntity._submitRegister} style={styles.registerbutton}>
              <Text>
                Register
              </Text>
            </Button></View>) :
            (<View style={{ flex: 1, padding:10, justifyContent: 'center' }}>
                <Text>
                  Registrasi berhasil
                </Text>
                <View>
                  <Button block onPress={()=>{NavigationService.navigate('Login');this.setState({isRegistered:false});}} style={styles.registerbutton}>
                    <Text>
                      Mulai Login
                    </Text>
                  </Button>
                </View>
              </View>
            )
        }
	        </Content>
	      </Container>
      </Drawer>
    );
  }
}

Drawer.defaultProps.styles.mainOverlay.elevation = 2;