import {StyleSheet, Dimensions} from 'react-native';
// import theme from '../../styles/theme.style';
// import {headingText, textInput} from '../../styles/common.style';
var width = Dimensions.get('window').width;

export default StyleSheet.create({
	home:{
		flex: 1, 
		//alignItems: 'center', 
		justifyContent: 'center',
		paddingTop:60,
		marginBottom:10,
	},
	hero:{
		paddingTop:20,
		paddingBottom:20,
		alignItems:'center',
		backgroundColor:'#FFFFFF',
		borderWidth:1,
		borderColor:"#EEEEEE"
	},
	whitetext:{
		color:'#FFFFFF',
		fontSize:20,
		fontWeight:'700',
	},
	ajardiaLogo:{
		 flex: 1,
	     resizeMode: 'contain',
	     width:200,
	},
	logoContainer:{
		height:100,
	},
	registerbutton:{
		marginTop:20,
		backgroundColor:'#18B5AE',
	},
	Tanggal:{
		width:width
	},
	formButtonUpload:{
		backgroundColor:"#18B5AE",
		marginTop:10,
	},
	textQuestion:{
		fontSize:15,
		marginTop:20,
		marginBottom:3
	},
});