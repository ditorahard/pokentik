import React, {Component} from 'react';
import { View, Image, Linking, ToastAndroid } from 'react-native';
import { Container, Content, Text, Body, Drawer } from "native-base";
import MainHeader from '../../components/Header/MainHeader.component';
import SideBar from '../../components/Sidebar/Sidebar.component';
import NotificationListItem from '../../components/ListItem/Notifications/NotificationListItem.component';
import styles from './WhatsappGroup.container.style';

export default class WhatsappGroup extends React.Component {
  
  closeDrawer = () => {
  this.drawer._root.close()
  };
  
  openDrawer = () => {
  this.drawer._root.open()
  };

  render() {

    const { navigation } = this.props;
    const grupKelurahan = navigation.getParam('grupKelurahan', null);
    const grupKecamatan = navigation.getParam('grupKecamatan', null);

    return (
      <Drawer
          ref={(ref) => { this.drawer = ref; }}
          content={<SideBar  />}
          onClose={() => this.closeDrawer()} >
        <MainHeader openDrawer={() => this.openDrawer()} title="Whatsapp Group" />
        <Container style={{ flex: 1, padding:10, alignItems:"flex-start" }}>
          <Content>
            {grupKelurahan ? 
              <NotificationListItem title={"Grup kelurahan"} content={"Grup whatsapp kelurahan"} action={()=>{Linking.openURL(grupKelurahan)}} />
                :
              <NotificationListItem title={"Grup kelurahan"} content={"Grup whatsapp kelurahan"} action={() => {ToastAndroid.show('User belum memiliki grup whatsapp kelurahan', ToastAndroid.SHORT);}} /> 
            }
            {grupKecamatan ?
              <NotificationListItem title={"Grup kecamatan"} content={"Grup whatsapp kecamatan"} action={()=>{Linking.openURL(grupKecamatan)}} /> 
              :
              <NotificationListItem title={"Grup kecamatan"} content={"Grup whatsapp kecamatan"} action={() => {ToastAndroid.show('User belum memiliki grup whatsapp kecamatan', ToastAndroid.SHORT);}} /> 
            }
          </Content>
        </Container>
      </Drawer>
    );
  }
}

Drawer.defaultProps.styles.mainOverlay.elevation = 2;
