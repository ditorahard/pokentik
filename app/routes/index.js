import React from 'react';
import { createStackNavigator, createSwitchNavigator } from 'react-navigation';
import HomeScreen from '../containers/Home/Home.container';
import LoginScreen from '../containers/Login/Login.container';
import ActivityListScreen from '../containers/Activity/List/ActivityList.container';
import RegistrationScreen from '../containers/Registration/Registration.container';
import ProfileScreen from '../containers/Profile/Profile.container';
import AddActivityScreen from '../containers/Activity/Add/AddActivity.container';
import NotificationsScreen from '../containers/Notifications/Notifications.container';
import HistoryScreen from '../containers/History/History.container';
import HowToUseScreen from '../containers/HowToUse/HowToUse.container';
import G1R1JScreen from '../containers/G1R1J/G1R1J.container';
import ActivityDetailScreen from '../containers/Activity/Detail/ActivityDetail.container';
import InfoScreen from '../containers/Info/Info.container';
import LogoutScreen from '../containers/Logout/Logout.container';
import WhatsappGroupScreen from '../containers/WhatsappGroup/WhatsappGroup.container';
import RedeemScreen from '../containers/Redeem/Redeem.container';
import ActivityChoicesScreen from '../containers/Activity/Choices/ActivityChoices.container';
import ForgotPasswordScreen from '../containers/ForgotPassword/Forgotpassword.container';

export const SignedIn = createStackNavigator(
   {
    Home: {
      screen: HomeScreen,
      navigationOptions:{
        header: null,
        // header: props => <MainHeader {...props} />,
      },
    },
    ActivityList: {
      screen:ActivityListScreen,
      navigationOptions:{
        header: null,
      },
    },
    Profile: {
      screen:ProfileScreen,
      navigationOptions:{
        header: null,
      },
    },
    AddActivity: {
      screen:AddActivityScreen,
      navigationOptions:{
        header: null,
      },
    },
    ActivityDetail: {
      screen:ActivityDetailScreen,
      navigationOptions:{
        header: null,
      },
    },
    ActivityChoices: {
      screen:ActivityChoicesScreen,
      navigationOptions:{
        header: null,
      },
    },
    Notifications: {
      screen:NotificationsScreen,
      navigationOptions:{
        header: null,
      },
    },
    History: {
      screen:HistoryScreen,
      navigationOptions:{
        header: null,
      },
    },
    HowToUse:{
      screen:HowToUseScreen,
      navigationOptions:{
        header: null,
      },
    },
    G1R1J:{
      screen:G1R1JScreen,
      navigationOptions:{
        header: null,
      },
    },
    Info:{
      screen:InfoScreen,
      navigationOptions:{
        header: null,
      },
    },
    WhatsappGroup:{
      screen:WhatsappGroupScreen,
      navigationOptions:{
        header: null,
      },
    },
    Redeem:{
      screen:RedeemScreen,
      navigationOptions:{
        header: null,
      },
    },
    Logout: {
      screen: LogoutScreen,
      navigationOptions:{
        header: null,
        // header: props => <MainHeader {...props} />,
      },
    },
  },
  {
    initialRouteName: 'Home',
  }
);

export const SignedOut = createStackNavigator({
  Login: {
      screen: LoginScreen,
      navigationOptions:{
        header: null,
        // header: props => <MainHeader {...props} />,
      },
    },
  Registration: {
      screen: RegistrationScreen,
      navigationOptions:{
        header: null,
        // header: props => <MainHeader {...props} />,
      },
    },
    ForgotPassword:{
      screen: ForgotPasswordScreen,
      navigationOptions:{
        header:null,
      },
    },
    SignIn: {
      screen: SignedIn,
      navigationOptions: {
        header:null
      }
    },
  },
  {
    initialRouteName: 'Login',
  },
);



export const createRootNavigator = (signedIn = false) => {
  return createSwitchNavigator(
    {
      SignedIn: {
        screen: SignedIn
      },
      SignedOut: {
        screen: SignedOut
      }
    },
    {
      initialRouteName: signedIn ? "SignedIn" : "SignedOut"
    }
    );
}