import { AsyncStorage } from 'react-native';

const deviceStorage = {
    // our AsyncStorage functions will go here :)

   async saveItem(key, value) {
    try {
      await AsyncStorage.setItem(key, value);
      // console.warn("save Item berhasil");
    } catch (error) {
      console.log('AsyncStorage Error: ' + error.message);
    }
  },
  async takeItem(key) {
  	var item='';
    try {
    	// console.warn("take item key", key);
      item = await AsyncStorage.getItem(key);
      // console.warn("item", item);
    } catch (error) {
      console.log('AsyncStorage Error: ' + error.message);
    }

    return item;
  },
  async removeItem(key) {
    try {
      await AsyncStorage.removeItem(key);
      // console.warn("remove Item berhasil");
      return true;
    }
    catch(exception) {
      return false;
    }
  },
  isSignedIn(){
    return new Promise((resolve, reject) => {
      AsyncStorage
        .getItem('token')
        .then(res => {
          if (res !== null) {
            resolve(true);
          } else {
            resolve(false);
          }
        })
        .catch(err => reject(err));
    });
  }
};

export default deviceStorage;