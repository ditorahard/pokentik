/** @format */

import {AppRegistry} from 'react-native';
import Pokentik from './app/index';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Pokentik);
